'use strict';

var DbOperations = new function() {
  const pool = require('./dbConfig.js');
  const jackPool = require('./jackDbConfig.js');

  this.getThreeeightCompanyCodes = async function() {
    var sql = "select code from companies where title in (select distinct title from (select distinct title from ask union select distinct title from bid) as sub)";
    var [companyResult, error] = await jackPool.query(sql);
    return companyResult;
  }

  this.insertCompany = async function(companyData) {
    var sql = "select * from companies where name = ?";
    var [result, error] = await pool.query(sql, [companyData.name]);

    var companyId;
    if(result.length == 0) {
      let sql = "INSERT INTO companies (name, ceo_name, registration_number, foundation_date, stock_issue_volume, employee_min, employee_max, site_url, office_number, address, about) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
      let values = [
        companyData.name,
        companyData.ceoName,
        companyData.businessNo,
        companyData.foundingDate.toString().substring(0, 4) + '-' + companyData.foundingDate.toString().substring(4, 6) + '-' + companyData.foundingDate.toString().substring(6, 8),
        companyData.totalShares,
        companyData.staffMin,
        companyData.staffMax,
        companyData.website,
        companyData.phoneNumber,
        companyData.address,
        companyData.about
      ];

      let [result, error] = await pool.query(sql, values);
      companyId = result.insertId;

      try {
        let key = 'companies/logo_' + companyId;
        let uploadResult = await utils.uploadFile(companyData.logo, key);
        let sql = "UPDATE companies SET logo_path = ? WHERE seq = ?";
        let [result, error] = await pool.query(sql, [uploadResult.Location, companyId]);
      } catch(err) {
        console.log('invalid logo ' + err);
      }
    } else {
      companyId = result[0].seq;
    }

    var sql = "select * from business_types where business_type = ?";
    var [result, error] = await pool.query(sql, [companyData.businessType]);
    var businessTypeId;
    if(result.length == 0) {
      var sql = "INSERT INTO business_types (business_type) VALUES (?)";
      var [result, error] = await pool.query(sql, [companyData.businessType]);
      businessTypeId = result.insertId;
    } else {
      businessTypeId = result[0].seq;
    }

    var sql = "select * from company_business_types where company_seq = ? and business_type_seq = ?";
    var [result, error] = await pool.query(sql, [companyId, businessTypeId]);
    if(result.length == 0) {
      var sql = "INSERT INTO company_business_types (company_seq, business_type_seq) VALUES (?, ?)";
      await pool.query(sql, [companyId, businessTypeId]);
    }

    for(var i = 0; companyData.lines.length > i; i++) {
      var businessLine = companyData.lines[i];

      var sql = "select * from business_lines where business_line = ?";
      var [result, error] = await pool.query(sql, [businessLine]);
      var businessLineId;
      if(result.length == 0) {
        var sql = "INSERT INTO business_lines (business_line) VALUES (?)";
        var [result, error] = await pool.query(sql, [businessLine]);
        businessLineId = result.insertId;
      } else {
        businessLineId = result[0].seq;
      }

      var sql = "select * from company_business_lines where company_seq = ? and business_line_seq = ?";
      var [result, error] = await pool.query(sql, [companyId, businessLineId]);
      if(result.length == 0) {
        var sql = "INSERT INTO company_business_lines (company_seq, business_line_seq) VALUES (?,?)";
        await pool.query(sql, [companyId, businessLineId]);
      }
    }

    var serviceTags = [];
    for(var i = 0; companyData.products.length > i; i++) {
      var product = companyData.products[i];
      serviceTags.push(product.name);
      var sql = "select * from products where company_seq = ? and name = ?";
      var [result, error] = await pool.query(sql, [companyId, product.name]);
      var productId;
      if(result.length == 0) {
        var sql = "INSERT INTO products (company_seq, name, summary, site_url, about) VALUES (?,?,?,?,?)";
        var [result, error] = await pool.query(sql, [companyId, product.name, product.header, product.website, product.description]);
        productId = result.insertId;

        try {
          let key = 'products/logo_' + productId;
          let uploadResult = await utils.uploadFile(product.logo, key);
          let sql = "UPDATE products SET logo_path = ? WHERE seq = ?";
          let [result, error] = await pool.query(sql, [uploadResult.Location, productId]);
        } catch(err) {
          console.log('invalid logo ' + err);
        }
      } else {
        productId = result[0].seq;
      }

      if(product.appStore) {
        var sql = "select * from product_social_links where product_seq = ? and social_service_seq = 3";
        var [result, error] = await pool.query(sql, [productId, product.name]);
        if(result.length == 0) {
          var sql = "INSERT INTO product_social_links (product_seq, social_service_seq, link_url) VALUES (?,?,?)";
          var [result, error] = await pool.query(sql, [productId, 3, product.appStore]);
        }
      }
      if(product.playStore) {
        var sql = "select * from product_social_links where product_seq = ? and social_service_seq = 2";
        var [result, error] = await pool.query(sql, [productId, product.name]);
        if(result.length == 0) {
          var sql = "INSERT INTO product_social_links (product_seq, social_service_seq, link_url) VALUES (?,?,?)";
          var [result, error] = await pool.query(sql, [productId, 2, product.playStore]);
        }
      }
      if(product.facebook) {
        var sql = "select * from product_social_links where product_seq = ? and social_service_seq = 1";
        var [result, error] = await pool.query(sql, [productId, product.name]);
        if(result.length == 0) {
          var sql = "INSERT INTO product_social_links (product_seq, social_service_seq, link_url) VALUES (?,?,?)";
          var [result, error] = await pool.query(sql, [productId, 1, product.facebook]);
        }
      }
    }

    for(var i = 0; companyData.investments.length > i; i++) {
      var investment = companyData.investments[i];
      var round;
      switch(investment.round) {
        case 'seed': round = 'SEED'; break;
        case 'pre-A': round = 'SEED'; break;
        case 'series A': round = 'SERIES_A'; break;
        case 'series B': round = 'SERIES_B'; break;
        case 'series C': round = 'SERIES_C'; break;
        case 'series D': round = 'SERIES_D'; break;
        case 'series E': round = 'SERIES_E'; break;
        case 'M&A': round = 'M&A'; break;
        default: round = 'PRIVATE';
      }
      var investmentDateParts = investment.date.toString().split('.');
      var investmentYear = investmentDateParts[0];
      var investmentMonth = investmentDateParts[1];
      if(investmentMonth.length == 1) investmentMonth = '0' + investmentMonth;
      var investmentDate = investmentYear + '-' + investmentMonth + '-01';

      var investmentRoundId;
      var sql = "select * from company_investments where company_seq = ? and investment_stage = ? and investment_date = ?";
      var [result, error] = await pool.query(sql, [companyId, round, investmentDate]);
      if(result.length == 0) {
        var sql = "INSERT INTO company_investments (company_seq, investment_stage, investment_amount, investment_date) VALUES (?,?,?,?)";
        var [result, error] = await pool.query(sql, [companyId, round, investment.amount, investmentDate]);
        investmentRoundId = result.insertId;
      } else {
        investmentRoundId = result[0].seq;
      }

      var majorInvestor = 0;
      if(companyData.investments.length-1 == i) {
        majorInvestor = 1;
      }
      for(var x = 0; investment.investors.length > x; x++) {
        var investor = investment.investors[x];
        var investorId;
        var sql = "select * from investors where name_kr = ?";
        var [result, error] = await pool.query(sql, [investor.name]);
        if(result.length == 0) {
          var sql = "INSERT INTO investors (name_kr, site_url) VALUES (?,?)";
          var [result, error] = await pool.query(sql, [investor.name, investor.website]);
          investorId = result.insertId;

          try {
            let key = 'investors/logo_' + investorId;
            let uploadResult = await utils.uploadFile(investor.logo, key);
            let sql = "UPDATE investors SET logo_path = ? WHERE seq = ?";
            let [result, error] = await pool.query(sql, [uploadResult.Location, investorId]);
          } catch(err) {
            console.log('invalid logo ' + err);
          }
        } else {
          investorId = result[0].seq;
        }

        var sql = "select * from company_investors where company_seq = ? and company_investment_seq = ? and investor_seq = ?";
        var [result, error] = await pool.query(sql, [companyId, investmentRoundId, investorId]);
        if(result.length == 0) {
          var sql = "INSERT INTO company_investors (company_seq, company_investment_seq, investor_seq, major_investor_flag) VALUES (?,?,?,?)";
          var [result, error] = await pool.query(sql, [companyId, investmentRoundId, investorId, majorInvestor]);
        }
      }
    }

    for(var i = 0; companyData.news.length > i; i++) {
      var newsObject = companyData.news[i];
      var sql = "select * from company_news where company_seq = ? and title = ? and media = ? and news_date = ?";
      var [result, error] = await pool.query(sql, [companyId, newsObject.headline, newsObject.media, newsObject.date]);
      if(result.length == 0) {
        var sql = "INSERT INTO company_news (company_seq, title, link_url, media, news_date) VALUES (?,?,?,?,?)";
        await pool.query(sql, [companyId, newsObject.headline, newsObject.link, newsObject.media, newsObject.date]);
      }
    }

    for(var i = 0; companyData.dartNotices.length > i; i++) {
      var dartNoticeObject = companyData.dartNotices[i];
      var sql = "select * from company_public_announcements where company_seq = ? and link_url = ? and announcement_date = ?";
      var [result, error] = await pool.query(sql, [companyId, dartNoticeObject.link, dartNoticeObject.date]);
      if(result.length == 0) {
        var sql = "INSERT INTO company_public_announcements (company_seq, title, link_url, submitter, announcement_date) VALUES (?,?,?,?,?)";
        await pool.query(sql, [companyId, dartNoticeObject.title, dartNoticeObject.link, dartNoticeObject.author, dartNoticeObject.date]);
      }
    }

    var sql = "select * from stocks where company_seq = ?";
    var [result, error] = await pool.query(sql, [companyId]);
    if(result.length == 0) {
      var sql = "INSERT INTO stocks (company_seq, open_flag, service_tags, slug) VALUES (?,?,?,?)";
      await pool.query(sql, [companyId, 1, serviceTags.join(','), companyData.baseName]);
    }
  }
}

// Public
module.exports = DbOperations;
