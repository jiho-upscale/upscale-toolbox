'use strict';

const dart = require('./dart.js');
const nps = require('./nps.js');
const jobplanet = require('./jobplanet.js');
const rocketpunch = require('./rocketpunch.js');
const thevc = require('./thevc.js');
const nice = require('./nice.js');
const saramin = require('./saramin.js');
const threeeight = require('./threeeight.js');
const utils = require('./utils.js');
const pool = require('./dbConfig.js');
const jackPool = require('./jackDbConfig.js');
const dbOperations = require('./dbOperations.js');
const inquirer = require('inquirer');

const checkSufficientInfo = async function(threeeightCode) {
  var results = null;
  var threeeightCompanyDetails = await threeeight.getCompanyDetailsByCode(threeeightCode);
  var niceResults = await nice.searchBaseName(threeeightCompanyDetails.companyBaseName);
  var niceResults;
  var niceCode;
  for(var i = 0; niceResults.length > i; i++) {
    niceCode = niceResults[i].code;
    let niceCompanyDetails = await nice.getCompanyDetails(niceCode);
    if(threeeightCompanyDetails.foundingDate == niceCompanyDetails.foundationDate || threeeightCompanyDetails.ceoName == niceCompanyDetails.ceoName) {
      niceResults = niceCompanyDetails;
      break;
    }
  }

  if(niceResults.name) {
    var theVcSearchResult = [];
    var theVcSearchResults = await thevc.search(threeeightCompanyDetails.companyBaseName);
    if(theVcSearchResults.length > 0) {
      theVcSearchResult = theVcSearchResults.filter(function(company) {
        return company.name.replace(/\s/g, '') == threeeightCompanyDetails.companyBaseName.replace(/\s/g, '');
      });
    }

    if(theVcSearchResult.length > 0) {
      var theVcCompanyDetails = await thevc.getCompanyDetails(theVcSearchResult[0].link);
      if(theVcCompanyDetails != null) {
        results = {
          name: niceResults.name,
          tid: niceResults.tid.replace(/-/g,''),
          niceCode: niceCode,
          niceCompanyDetails: niceResults,
          theVcCompanyDetails: theVcCompanyDetails,
          threeeightCompanyDetails: threeeightCompanyDetails
        }
      }
    }
  }

  return results;
}

const run = async function() {
  var companies = await dbOperations.getThreeeightCompanyCodes();
  //var companies = [{code: 234690}];
  for(var i = 0; companies.length > i; i++) {
    var checkInfo = await checkSufficientInfo(companies[i].code);
    if(checkInfo == null) {
      console.log('Not enough data exists for company ' + companies[i].code);
      continue;
    }

    var companyNames = [checkInfo.name];
    var startup = {};

    var sql = "select * from companies where name = ?";
    var [result, error] = await pool.query(sql, [companyNames[0]]);
    if(result.length > 0) {
      console.log('Company already exists in databse ' + companyNames[0]);
      continue;
    }

    var niceCode = checkInfo.niceCode;
    var niceCompanyDetails = checkInfo.niceCompanyDetails;

    startup.name = companyNames[0];
    startup.ceoName = niceCompanyDetails.ceoName;
    startup.address = niceCompanyDetails.address;
    startup.businessType = niceCompanyDetails.industry;
    startup.foundingDate = niceCompanyDetails.foundationDate.replace(/\D/g,'');
    startup.phoneNumber = '';
    startup.logo = '';
    startup.investments = [];
    startup.website = '';
    startup.about = '';
    startup.products = [];
    startup.baseName = utils.getCompanyBaseName(companyNames[0]);
    startup.news = [];
    startup.staffMin = niceCompanyDetails.staffMin;
    startup.staffMax = niceCompanyDetails.staffMax;
    startup.businessNo = niceCompanyDetails.tid.replace(/-/g,'');
    startup.dartNotices = [];
    startup.lines = [];
    var tid = niceCompanyDetails.tid.replace(/\D/g,'');

    startup.totalShares = 0;
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      var capDetails = checkInfo.threeeightCompanyDetails;
      if(capDetails.totalShares) startup.totalShares = capDetails.totalShares;
    }

    var companyCode = await dart.search(startup.baseName);
    if(companyCode != null) {
      var dartInfo = await dart.getCompanyInfo(companyCode);
      startup.ceoName = dartInfo.ceo_nm;
      startup.foundingDate = dartInfo.est_dt;
      startup.businessNo = dartInfo.bsn_no;
      startup.address = dartInfo.adr;
      startup.website = dartInfo.hm_url;
      startup.phoneNumber = dartInfo.phn_no;
      startup.dartNotices = await dart.getCompanyNotice(companyCode);
    }

    var npsSearchResults = await nps.searchCompany(companyNames[0], tid);
    if(npsSearchResults) {
      var npsCompanyDetails = await nps.getCompanyDetails(npsSearchResults.seq);
      var range = utils.getStaffRange(npsCompanyDetails.employeeCount);
      startup.staffMin = range.min;
      startup.staffMax = range.max;
      startup.businessNo = npsCompanyDetails.tid;
      if(startup.foundingDate.length != 8) startup.foundingDate = npsCompanyDetails.foundingDate.replace(/\D/g,'');
    }

    var productNames = [];
    var theVcCompanyDetails = checkInfo.theVcCompanyDetails;
    if(theVcCompanyDetails != null) {
      if(theVcCompanyDetails.logo) startup.logo = theVcCompanyDetails.logo;
      if(theVcCompanyDetails.investmentRounds) startup.investments = theVcCompanyDetails.investmentRounds;
      if(theVcCompanyDetails.website) startup.website = theVcCompanyDetails.website;
      if(theVcCompanyDetails.address) startup.address = theVcCompanyDetails.address;
      if(theVcCompanyDetails.news) startup.news = theVcCompanyDetails.news;
      if(theVcCompanyDetails.tags) startup.lines = theVcCompanyDetails.tags;

      for(var x = 0; theVcCompanyDetails.products.length > x; x++) {
        productNames.push(theVcCompanyDetails.products[x].name);
      }
      if(theVcCompanyDetails.products) startup.products = theVcCompanyDetails.products;
    }

    var jobplanetSearchResults = [];
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      jobplanetSearchResults = await jobplanet.search(thisCompanyName);
      if(jobplanetSearchResults.length > 0) break;
    }
    if(jobplanetSearchResults.length > 0) {
      var jobplanetSearchResult = jobplanetSearchResults.filter(function(company) {
        var baseName = utils.getCompanyBaseName(company.name);
        return baseName.replace(/\s/g, '') == startup.baseName.replace(/\s/g, '');
      });

      if(jobplanetSearchResult.length > 0) {
        var jobplanetCompanyDetails = await jobplanet.getCompanyDetails(jobplanetSearchResult[0].link);
        if(startup.logo.length == 0) startup.logo = jobplanetCompanyDetails.logo;
        if(jobplanetCompanyDetails.description != null && jobplanetCompanyDetails.description.length > 5) {
          startup.about = jobplanetCompanyDetails.description;
        }
      }
    }

    var rocketpunchLink = null;
    var rocketpunchCompanyName;
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      rocketpunchLink = await rocketpunch.findCompanyDetailPage(thisCompanyName, productNames);
      if(rocketpunchLink != null) {
        rocketpunchCompanyName = thisCompanyName;
        break;
      }
    }

    if(rocketpunchLink != null) {
      var rocketPunchCompanyDetails = await rocketpunch.getCompanyDetails(rocketpunchLink);

      if(rocketPunchCompanyDetails.products.length > 0) {
        var deDuplicatedProducts = [];
        if(startup.products.length > 0) {
          for(var y = 0; startup.products.length > y; y++) {
            var productName = startup.products[y].name;
            var matchProduct = rocketPunchCompanyDetails.products.filter(function(object) {
              return object.name.replace(/\s/g,'') == productName.replace(/\s/g,'');
            });
            if(matchProduct.length == 0) {
              deDuplicatedProducts.push(startup.products[y]);
            }
          }
        }
        startup.products = deDuplicatedProducts.concat(rocketPunchCompanyDetails.products);
      }

      if(rocketPunchCompanyDetails.news.length > 0) {
        var deDuplicatedNews = [];
        if(startup.news.length > 0) {
          for(var y = 0; startup.news.length > y; y++) {
            var newsLink = startup.news[y].link;
            var matchNews = rocketPunchCompanyDetails.news.filter(function(object) {
              return object.link.trim() == newsLink.trim();
            });
            if(matchNews.length == 0) {
              deDuplicatedNews.push(startup.news[y]);
            }
          }
        }
        startup.news = deDuplicatedNews.concat(rocketPunchCompanyDetails.news);
      }
      startup.news.sort(function(a, b) {
        return b.date.replace(/-/g, '') - a.date.replace(/-/g, '');
      });

      if(rocketPunchCompanyDetails.tags.length > 0) startup.lines = Array.from(new Set(startup.lines.concat(rocketPunchCompanyDetails.tags)));
      if(!startup.logo) startup.logo = rocketPunchCompanyDetails.logo;
      if(rocketPunchCompanyDetails.description != null && startup.about.length < rocketPunchCompanyDetails.description.length) startup.about = rocketPunchCompanyDetails.description;
      if(rocketPunchCompanyDetails.phoneNumber) startup.phoneNumber = rocketPunchCompanyDetails.phoneNumber;

      for(var x = 0; startup.products.length > x; x++) {
        var product = startup.products[x];
        var matchProduct = [];
        if(theVcCompanyDetails) {
          matchProduct = theVcCompanyDetails.products.filter(function(object) {
            return object.name.replace(/\s/g,'') == product.name.replace(/\s/g,'');
          });
        }
        if(matchProduct.length > 0) {
          startup.products[x].logo = matchProduct[0].logo;
        } else {
          startup.products[x].logo = '';
        }
      }
    }

    console.log(startup);
    //await dbOperations.insertCompany(startup);
  }
};

run().then(function() {
  console.log('done');
  pool.end();
});
