'use strict';

const pool = require('./dbConfig.js');
const jackPool = require('./jackDbConfig.js');
const utils = require('./utils.js');
const toast = require('./toast.js');

const run = async function() {

  // phone numbers that received messages yesterday
  var matchLogSql = `select distinct phone_number from match_logs where date_format(datetime, '%Y-%m-%d') = date_format(NOW() - INTERVAL 1 DAY, '%Y-%m-%d')`;
  var [received, error] = await jackPool.query(matchLogSql);

  // 오늘 신규 주문 넣은 사람들 가져오기
  var yesterdaysOrdersSql = `select
                              c.seq,
                              c.name,
                              s.slug,
                              CAST(sum(so.price*so.volume)/sum(so.volume) AS UNSIGNED) as avgPrice,
                            	so.order_type,
                            	c.name,
                              ud.user_seq as userId,
                            	ud.phone_number,
                            	count(distinct so.order_type) as orders
                          from stock_orders as so
                          join stocks as s
                          on so.stock_seq = s.seq
                          join companies as c
                          on s.company_seq = c.seq
                          join user_details as ud
                          on so.user_seq = ud.user_seq
                          where date_format(so.order_at, '%Y-%m-%d') = date_format(NOW(), '%Y-%m-%d')
                          group by c.name, ud.phone_number
                          having count(distinct so.order_type) = 1
                          order by ud.phone_number, c.name`;

  var [yesterdaysOrders, error] = await pool.query(yesterdaysOrdersSql);

  var receivers = {};
  for(var i=0; yesterdaysOrders.length > i; i++) {
    let userId = yesterdaysOrders[i].userId;
    let userPhoneNumber = yesterdaysOrders[i].phone_number;
    let companyId = yesterdaysOrders[i].seq;
    let companyName = yesterdaysOrders[i].name;
    let companySlug = yesterdaysOrders[i].slug;
    let averagePrice = yesterdaysOrders[i].avgPrice;
    //let targetOrder = yesterdaysOrders[i].order_type == 'BUY' ? 'SELL' : 'BUY';
    let requestor = yesterdaysOrders[i].phone_number;

    //if(received.find(row => row.phone_number === userPhoneNumber)) continue;

    // 해당 유저가 올린 종목의 정보 가져오기
    let todaysOrdersSql = `select
                            so.stock_seq,
                            so.order_type,
                            min(so.price) as minPrice,
                            max(so.price) as maxPrice,
                            CAST(sum(so.price*so.volume)/sum(so.volume) AS UNSIGNED) as avgPrice,
                            sum(so.volume) as totalVolume,
                            count(distinct ud.seq) as numberOfUsers
                          from stock_orders as so
                          join stocks as s
                          on so.stock_seq = s.seq
                          join user_details as ud
                          on so.user_seq = ud.user_seq
                          where date_format(so.order_at, '%Y-%m-%d') = date_format(NOW(), '%Y-%m-%d')
                          and s.company_seq = ?
                          group by so.stock_seq, so.order_type
                          order by totalVolume desc`;

    var [todaysOrders, error] = await pool.query(todaysOrdersSql, [companyId]);
    for(var j=0; todaysOrders.length > j; j++) {
      let minPrice = todaysOrders[j].minPrice;
      let maxPrice = todaysOrders[j].maxPrice;
      let avgPrice = todaysOrders[j].avgPrice;
      let totalVolume = todaysOrders[j].totalVolume;
      let numberOfUsers = todaysOrders[j].numberOfUsers;
      let orderString = todaysOrders[j].order_type == 'BUY' ? '매수' : '매도';

      if(!receivers[userPhoneNumber]) {
        receivers[userPhoneNumber] = {};
      }
      if(!receivers[userPhoneNumber][companyName]) {
        receivers[userPhoneNumber][companyName] = {};
      }
      if(!receivers[userPhoneNumber][companyName].match) {
        receivers[userPhoneNumber][companyName].match = {};
      }
      receivers[userPhoneNumber][companyName].orderPrice = averagePrice;

      if(!receivers[userPhoneNumber][companyName].match[orderString]) {
        receivers[userPhoneNumber][companyName].match[orderString] = {};
      }
      if(!receivers[userPhoneNumber][companyName].slug) {
        receivers[userPhoneNumber][companyName].slug = companySlug;
      }
      receivers[userPhoneNumber][companyName].match[orderString].minPrice = minPrice;
      receivers[userPhoneNumber][companyName].match[orderString].maxPrice = maxPrice;
      receivers[userPhoneNumber][companyName].match[orderString].avgPrice = avgPrice;
      receivers[userPhoneNumber][companyName].match[orderString].totalVolume = totalVolume;
      receivers[userPhoneNumber][companyName].match[orderString].numberOfUsers = numberOfUsers;
    }
  }

  var body = {};
  var shortUrlCache = {};
  for(var userPhoneNumber in receivers) {
    if (receivers.hasOwnProperty(userPhoneNumber)) {
      let orderCompanies = Object.keys(receivers[userPhoneNumber]);

      var header = "(광고)" + userPhoneNumber.substr(userPhoneNumber.length-4, 4) + "님께서 금일 신청하신 비상장 주식 매매 ?건에 대한 요약 보고입니다!\n";
      let longUrl = 'https://upscale.kr/signup?phone=' + userPhoneNumber;
      let shortUrlResults = await utils.getShortUrl(longUrl);
      var footer = "\n앞으로도 요약 정보 받아 보시겠습니까?\n * 무료구독:" + shortUrlResults.link + "\n\n[무료 수신거부]0808880308";

      for(var companyName in receivers[userPhoneNumber]) {
        if(receivers[userPhoneNumber].hasOwnProperty(companyName)) {
          let orderTypes = Object.keys(receivers[userPhoneNumber][companyName].match);
          let orderType = orderTypes[0];

          if(!body[userPhoneNumber]) {
            body[userPhoneNumber] = header;
          }

          let thisStock = "";
          thisStock += "\n[" + companyName + "]\n"
          thisStock += " - 신청하신 " + orderType + " 평균가: " + receivers[userPhoneNumber][companyName].orderPrice.toLocaleString() + "원\n"
          if(receivers[userPhoneNumber][companyName].match['매도']) {
            thisStock += " - 금일 매도 호가: " + receivers[userPhoneNumber][companyName].match['매도'].minPrice.toLocaleString() + "원\n"
            thisStock += " - 금일 매도 신청자: " + receivers[userPhoneNumber][companyName].match['매도'].numberOfUsers + "명\n"
          } else {
            thisStock += " - 금일 매도 호가: 없음\n"
            thisStock += " - 금일 매도 신청자: 0명\n"
          }
          if(receivers[userPhoneNumber][companyName].match['매수']) {
            thisStock += " - 금일 매수 호가: " + receivers[userPhoneNumber][companyName].match['매수'].maxPrice.toLocaleString() + "원\n"
            thisStock += " - 금일 매수 신청자: " + receivers[userPhoneNumber][companyName].match['매수'].numberOfUsers + "명\n"
          } else {
            thisStock += " - 금일 매수 호가: 없음\n"
            thisStock += " - 금일 매수 신청자: 0명\n"
          }

          if(!shortUrlCache[companyName]) {
            let longUrl = 'https://upscale.kr/trade/' + receivers[userPhoneNumber][companyName].slug;
            let shortUrlResults = await utils.getShortUrl(longUrl);
            shortUrlCache[companyName] = shortUrlResults.link;
          }
          thisStock += " * 보기: " + shortUrlCache[companyName] + "\n";

          var cumulativeBytes = utils.getEucKrByte(body[userPhoneNumber]);
          var currentBytes = utils.getEucKrByte(thisStock+footer);
          if(cumulativeBytes+currentBytes <= 2000) {
            body[userPhoneNumber] += thisStock;
          }
        }
      }
      body[userPhoneNumber] = body[userPhoneNumber].replace(/\?/g, orderCompanies.length.toString());
      body[userPhoneNumber] += footer;
    }
  }

  let phoneNumbers = Object.keys(body);
  console.log('send alerts to ' + phoneNumbers.length + ' users');

  for(var userPhoneNumber in body) {
    if (body.hasOwnProperty(userPhoneNumber)) {
      let areaCode = userPhoneNumber.substring(0, 3);
      if(['010', '016', '017', '018', '019'].includes(areaCode)) {
        console.log('sending to ' + userPhoneNumber);
        var sendResult = await toast.sendSms('비상장주시세알림', body[userPhoneNumber], [userPhoneNumber]);
        var phoneNumber = sendResult.body.data.sendResultList[0].recipientNo;
        var resultMessage = sendResult.body.data.sendResultList[0].resultMessage;
        var sql = "insert into match_logs (phone_number, message, result_message, raw_result) values (?,?,?,?)";
        var [userResult, error] = await jackPool.query(sql, [phoneNumber, body[userPhoneNumber], resultMessage, JSON.stringify(sendResult.body.data.sendResultList)]);
      }
    }
  }
}

run().then(function() {
  console.log('done');
  pool.end();
  jackPool.end();
});
