'use strict';

var Nice = new function() {

  const request = require('request-promise-native');
  const socksAgent = require('socks-proxy-agent');
  const useragent = require('random-useragent');
  const cheerio = require("cheerio");
  const utils = require('./utils.js');
  const queryEncode = require("querystring").encode;
  const he = require('he');
  const host = 'https://www.nicebizinfo.com';
  const retryLimit = 50;
  var retryCount = 0;
  var currentProxy;

  const endpoint = {
    searchCompany: '/ep/EP0100M001GE.nice',
    companyDetail: '/ep/EP0100M002GE.nice'
  }

  var getProxy = async function() {
    try {
      var proxyInfo = await utils.getProxy();
      //var agent = new proxy(proxyInfo.curl);
      var agent = new socksAgent(proxyInfo.curl);
      currentProxy = proxyInfo.curl;
      console.log('[nice] proxy is: ' + currentProxy);
      return agent;
    } catch(error) {
      console.log(error);
      return getProxy();
    }
  }

  var callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    console.log('[nice] call ' + url);

    var options = {
      method: method.toUpperCase(),
      url: url,
      headers: {
        'User-Agent': useragent.getRandom(function(data) {
          if(data.folder == '/Browsers - Mac') return true;
          else return false;
        })
      },
      agent: await getProxy(),
      timeout: 3000,
      resolveWithFullResponse: true
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }

    var result = request(options).then(function(result) {
      if(result == null || typeof result == 'undefined') {
        throw result;
      } else {
        retryCount = 0;
        return result.body;
      }
    }).catch(function(error) {
      if(error.response && (error.response.statusCode == 404 || error.response.statusCode == 500)) {
        throw error;
      }

      var badProxies = utils.cache().get('badProxies');
      if(badProxies == null) badProxies = [];
      badProxies.push(currentProxy);
      utils.cache().set('badProxies', badProxies);

      if(retryLimit > retryCount) {
        retryCount++;
        console.log('retry nice : ' + retryCount);
        return callRawWebsite(method, endpoint, query, body);
      } else {
        console.log(error);
        process.exit();
      }
    });
    return Promise.resolve(result);
  }
  this.callRawWebsite = callRawWebsite;

  this.search = async function(companyName) {
    var rawOutput = await this.callRawWebsite('POST', endpoint.searchCompany, {itgSrch: companyName}, null);
    var htmlObject = cheerio.load(rawOutput);
    var rows = htmlObject('div.cTable.sp3.mb60 > table > tbody > tr.bg');
    //var rows = htmlObject('div.cTable.sp3.mb60 > table > tbody > tr.bg > th.tal.bdl1');

    var results = [];
    for(var i = 0; rows.length > i; i++) {
      var result = {};
      var row = cheerio(rows[i]).find('th.tal.bdl1');
      var subRow = cheerio(rows[i]).next();
      var typeImg = row.find('img').attr('src');

      if(typeImg == '/res/img/txt/txt_search12.png') continue; // 피흡수합병
      if(typeImg == '/res/img/txt/txt_search13.png') continue; // 폐업 법인

      var name = row.find('span.fz14.fwb.ml10.fErr > a').text();
      var industry = subRow.find('span.fll.textLeftBg.gear > span').text();
      var address = subRow.find('li.addr').text();

      if(companyName.trim() == name.trim()) {
        result.name = he.decode(name);
        result.industry = he.decode(industry);
        result.address = he.decode(address);
        var regex = /.*\('(.*)'\)/g;
        var match = regex.exec(row.find('span.fz14.fwb.ml10.fErr > a').attr('onclick'));
        result.code = he.decode(match[1]);
        results.push(result);
      }
    }
    return results;
  }

  this.searchBaseName = async function(companyBaseName) {
    var rawOutput = await this.callRawWebsite('POST', endpoint.searchCompany, {itgSrch: companyBaseName}, null);
    var htmlObject = cheerio.load(rawOutput);
    var rows = htmlObject('div.cTable.sp3.mb60 > table > tbody > tr.bg');

    var results = [];
    for(var i = 0; rows.length > i; i++) {
      var result = {};
      var row = cheerio(rows[i]).find('th.tal.bdl1');
      var subRow = cheerio(rows[i]).next();
      var typeImg = row.find('img').attr('src');

      if(typeImg == '/res/img/txt/txt_search12.png') continue; // 피흡수합병
      if(typeImg == '/res/img/txt/txt_search13.png') continue; // 폐업 법인

      var name = row.find('span.fz14.fwb.ml10.fErr > a').text();
      var industry = subRow.find('span.fll.textLeftBg.gear > span').text();
      var address = subRow.find('li.addr').text();
      if(companyBaseName.trim() == utils.getCompanyBaseName(name.trim())) {
        result.name = he.decode(name);
        result.industry = he.decode(industry);
        result.address = he.decode(address);
        var regex = /.*\('(.*)'\)/g;
        var match = regex.exec(row.find('span.fz14.fwb.ml10.fErr > a').attr('onclick'));
        result.code = he.decode(match[1]);
        results.push(result);
      }
    }
    return results;
  }

  this.getCompanyDetails = async function(code) {
    var rawOutput = await this.callRawWebsite('GET', endpoint.companyDetail, {kiscode: code}, null);
    var htmlObject = cheerio.load(rawOutput);

    var result = {};
    result.name = he.decode(htmlObject('div.header > h1').text().trim());
    result.staffMin = 0;
    result.staffMax = 0;

    var employeeInfo = htmlObject('img[alt=연봉현황]').parents('div.cSection').find('div.iconBox.bg6.tar strong').text().trim();
    if(employeeInfo != '-' && employeeInfo.length > 0) {
      var employeeInfoParts = employeeInfo.split('~');
      var regex1 = /([0-9,]{1,6})명/g;
      var min = regex1.exec(employeeInfoParts[0].trim());
      result.staffMin = min[1].replace(/\D/g,'');

      if(employeeInfoParts.length == 2) {
        var regex2 = /([0-9,]{1,6})명/g;
        var max = regex2.exec(employeeInfoParts[1].trim());
        result.staffMax = max[1].replace(/\D/g,'');
      } else {
        result.staffMax = '0';
      }
    }

    var infoCells = htmlObject('div.cTable.sp2.mb10 > table > tbody td > div.iconBox');
    for(var i = 0; infoCells.length > i; i++) {
      var label = he.decode(cheerio(infoCells[i]).find('p').text().trim());
      var data = he.decode(cheerio(infoCells[i]).find('strong').text().trim());

      switch(label) {
        case '대표자': result.ceoName = data.trim(); break;
        case '본사주소': result.address = data.trim(); break;
        case '사업자번호': result.tid = data.trim(); break;
        case '산업': result.industry = data.trim(); break;
        case '설립일자': result.foundationDate = data.trim(); break;
      }
    }
    return result;
  }
}

// Public
module.exports = Nice;
