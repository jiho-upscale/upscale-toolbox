'use strict';

var Naver = new function() {
  const request = require('request-promise-native');
  const crypto = require('crypto');
  const moment = require("moment");
  const queryEncode = require("querystring").encode;
  const host = 'https://api.naver.com';

  const apiUserId = 1680416;
  const apiKey = '01000000003639f76508871d8d36b9cd33fa2219d9c94e906a8553b7cc2a0455461eb1c9b7';
  const apiSecret = 'AQAAAAA2OfdlCIcdjTa5zTP6IhnZZ9PCqzsUG9ja41N10YN9Sw==';

  const endpoint = {
    getAvgKeywordBid: '/estimate/average-position-bid/keyword',
    getMedKeywordBid: '/estimate/median-bid/keyword',
    getMedKeywordBid: '/estimate/median-bid/keyword',
    getKeywordPerformance: '/estimate/performance-bulk/keyword',
    channel: '/ncc/channels',
    keywords: '/ncc/keywords'
  }

  this.getApiUserId = function() {
    return apiUserId;
  }

  this.callApi = async function(method, endpoint, getQuery, bodyQuery) {
    if(getQuery == null) getQuery = {};
    if(bodyQuery == null) bodyQuery = {};

    let options = {
      method: method.toUpperCase(),
      //json: true
    };

    let requestUri;
    if(method.toUpperCase() == 'GET') {
      options.url = host + endpoint;
      options.qs = getQuery;
    } else if(method.toUpperCase() == 'POST') {
      options.url = host + endpoint + '?' + queryEncode(getQuery);
      //options.body = bodyQuery;
      options.body = JSON.stringify(bodyQuery);
    }

    let nonce = Date.now();
    let hash = crypto.createHmac('sha256', apiSecret);
    let signString = nonce + '.' + method + '.' + endpoint;
    hash.update(signString);
    let sign = hash.digest('base64');

    options.headers = {
      'content-type': 'application/json',
      'X-Timestamp': nonce,
      'X-API-KEY': apiKey,
      'X-Customer': apiUserId,
      'X-Signature': sign
    }

    try {
      let result = await request(options);
      return JSON.parse(result);
    } catch(e) {
      console.log(e);
    }
  }

  this.createChannel = async function(name, url) {
    let payload = {
      businessInfo: {
        site: url
      },
      channelTp: 'SITE',
      name: name,
    };
    var result = await this.callApi('POST', endpoint.channel, null, payload);
    return result;
  }

  this.getAvgKeywordBid = async function(items, platform) {
    var result = await this.callApi('POST', endpoint.getAvgKeywordBid, null, {device: platform, items: items});
    return result;
  }

  this.getKeywordsByAdGroup = async function(addGroup) {
    var result = await this.callApi('GET', endpoint.keywords, {nccAdgroupId: addGroup}, null);
    return result;
  }

  this.createKeywordAd = async function(keywords, addGroup) {
    var result = await this.callApi('POST', endpoint.keywords, {nccAdgroupId: addGroup}, {nccKeywords: keywords});
    return result;
  }
}

// Public
module.exports = Naver;
