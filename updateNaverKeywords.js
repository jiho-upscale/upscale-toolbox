'use strict';

const utils = require('./utils.js');
const pool = require('./dbConfig.js');
const jackPool = require('./jackDbConfig.js');
const naver = require('./naver.js');

var run = async function() {
  let getStocksSql = `select * from stocks where open_flag = 1`;
  let [stockResults, stockError] = await pool.query(getStocksSql);

  let additionalKeywords = [
    '주식',
    '장외주식',
    '비상장주식',
    '주식거래',
    '시세',
    '주가',
    '매매'
  ];

  let filteredKeywordSets = [];
  let filteredKeywordSetsCount = 0;
  for(let x = 0; additionalKeywords.length > x; x++) {
    let keywordSet = [];
    let slugs = [];
    let keywordSetCount = 0;
    for(let i = 0; stockResults.length > i; i++) {
      if(!keywordSet[keywordSetCount]) keywordSet[keywordSetCount] = [];
      let keyword = stockResults[i].slug + additionalKeywords[x];
      keywordSet[keywordSetCount].push({key: keyword, position: 1});
      slugs[keyword] = stockResults[i].slug;
      if((i+1)%200 == 0) keywordSetCount++;
    }

    for(let i = 0; keywordSet.length > i; i++) {
      let bidResults = await naver.getAvgKeywordBid(keywordSet[i], 'MOBILE');
      for(let y = 0; bidResults.estimate.length > y; y++) {
        if(bidResults.estimate[y].bid <= 300) {
          if(!filteredKeywordSets[filteredKeywordSetsCount]) filteredKeywordSets[filteredKeywordSetsCount] = [];
          let keywordItem = bidResults.estimate[y];
          let keyword = bidResults.estimate[y].keyword;
          keywordItem.link = 'https://upscale.kr/trade/' + slugs[keyword];
          filteredKeywordSets[filteredKeywordSetsCount].push(keywordItem);
          if((y+1)%100 == 0) filteredKeywordSetsCount++;
        }
      }
    }
  }

  let keywords = await naver.getKeywordsByAdGroup('grp-a001-01-000000012329529');
  for(let i = 0; filteredKeywordSets.length > i; i++) {
    for(let x = 0; filteredKeywordSets[i].length > x; x++) {
      let keyword = filteredKeywordSets[i][x];
      let payload = [{
        bidAmt: keyword.bid,
        keyword: keyword.keyword,
        links: {
          pc: { final: keyword.link},
          mobile: { final: keyword.link }
        },
        nccAdgroupId: 'grp-a001-01-000000012329529',
        nccCampaignId: 'cmp-a001-01-000000002261022'
      }];
      let createKeywordResult = await naver.createKeywordAd(payload, 'grp-a001-01-000000012329529');
      console.log(createKeywordResult);
      process.exit();
    }

  }
}

run().then(function() {
  pool.end();
  jackPool.end();
});
