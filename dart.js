'use strict';

var Dart = new function() {

  const request = require('request-promise-native');
  const cheerio = require("cheerio");
  const moment = require("moment");
  const queryEncode = require("querystring").encode;
  const host = 'http://dart.fss.or.kr';

  const endpoint = {
    companyNotice: '/api/search.json',
    companyInfo: '/api/company.json',
    searchCompany: '/corp/searchCorpL.ax',
  }

  const apiKey = '2021c1893b5260e09511a361f57cd0c212a88e68';

  this.callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    var options = {
      method: method.toUpperCase(),
      url: url
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }
    var result = await request(options);
    return result;
  }

  this.callApi = async function(method, endpoint, query) {
    if(query == null) query = {};
    query.auth = apiKey;

    var options = {
      method: method.toUpperCase(),
    };

    if(method.toUpperCase() == 'GET') {
      options.url = host + endpoint + '?' + queryEncode(query);
    } else if(method.toUpperCase() == 'POST') {
      options.url = host + endpoint;
      options.formData = query;
    }

    console.log('[dart] call ' + options.url);
    var result = await request(options);
    if(result === Object(result)) return result;
    else return JSON.parse(result);
  }

  this.search = async function(companyName) {
    var rawOutput = await this.callRawWebsite('POST', endpoint.searchCompany, {textCrpNm: companyName, currentPage: 1}, null);
    var htmlObject = cheerio.load(rawOutput);
    var searchResults = htmlObject('tr');
    var companyCode = null;

    await searchResults.each(function(i, elm) {
      var resultName = cheerio(this).find("input[name='hiddenCikNM1']").val();
      if(typeof resultName !== 'undefined' && resultName != null && resultName.trim() == companyName.trim()) {
        companyCode = cheerio(this).find("input[name='hiddenCikCD1']").val();
        return false;
      }
    });
    return companyCode;
  }

  this.getCompanyInfo = async function(companyCode) {
    var companyInfo = await this.callApi('GET', endpoint.companyInfo, {crp_cd: companyCode});
    return companyInfo;
  }

  this.getCompanyNotice = async function(companyCode) {
    var rawResults = [];
    var pageSet = 100;
    var totalPages = 0;
    var currentPage = 1;
    while(totalPages == 0 || totalPages >= currentPage) {
      let params = {
        crp_cd: companyCode,
        start_dt: 19990101,
        page_no: currentPage,
        page_set: pageSet,
        series: 'asc'
      };
      let companyInfo = await this.callApi('GET', endpoint.companyNotice, params);
      if(companyInfo.total_count == 0) break;
      if(totalPages == 0) {
        totalPages = Math.ceil(companyInfo.total_count/pageSet);
      }
      rawResults = rawResults.concat(companyInfo.list);
      currentPage++;
    }

    var results = [];
    for(var i = 0; rawResults.length > i; i++) {
      results.push({
        link: 'http://dart.fss.or.kr/dsaf001/main.do?rcpNo=' + rawResults[i].rcp_no,
        date: moment(rawResults[i].rcp_dt, "YYYYMMDD").format("YYYY-MM-DD"),
        title: rawResults[i].rpt_nm,
        author: rawResults[i].flr_nm,
      });
    }

    return results;
  }
}

// Public
module.exports = Dart;
