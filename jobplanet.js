'use strict';

var Jobplanet = new function() {

  const request = require('request-promise-native');
  const socksAgent = require('socks-proxy-agent');
  const useragent = require('random-useragent');
  const cheerio = require("cheerio");
  const utils = require('./utils.js');
  const queryEncode = require("querystring").encode;
  const he = require('he');
  const host = 'https://www.jobplanet.co.kr';
  const retryLimit = 100;
  var retryCount = 0;
  var currentProxy;

  const endpoint = {
    searchCompany: '/search/companies'
  }

  var getProxy = async function() {
    try {
      var proxyInfo = await utils.getProxy();
      //var agent = new proxy(proxyInfo.curl);
      var agent = new socksAgent(proxyInfo.curl);
      currentProxy = proxyInfo.curl;
      console.log('[jobplanet] proxy is: ' + currentProxy);
      return agent;
    } catch(error) {
      console.log(error);
      return getProxy();
    }
  }

  var callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    console.log('[jobplanet] call ' + url);

    var headers = {
      'User-Agent': useragent.getRandom(function(data) {
        if(data.folder == '/Browsers - Mac') return true;
        else return false;
      })
    };

    var options = {
      method: method.toUpperCase(),
      url: url,
      headers: headers,
      agent: await getProxy(),
      timeout: 3000,
      resolveWithFullResponse: true
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }

    var result = request(options).then(function(result) {
      if(result == null || typeof result == 'undefined') {
        throw result;
      } else {
        retryCount = 0;
        return result.body;
      }
    }).catch(function(error) {
      if(error.response && (error.response.statusCode == 404 || error.response.statusCode == 500)) {
        throw error;
      }

      var badProxies = utils.cache().get('badProxies');
      if(badProxies == null) badProxies = [];
      badProxies.push(currentProxy);
      utils.cache().set('badProxies', badProxies);

      if(retryLimit > retryCount) {
        retryCount++;
        console.log('retry jobplanet : ' + retryCount);
        return callRawWebsite(method, endpoint, query, body);
      } else {
        throw error;
      }
    });
    return Promise.resolve(result);
  }
  this.callRawWebsite = callRawWebsite;

  this.search = async function(companyName) {
    var url = endpoint.searchCompany + '/' + encodeURI(companyName);
    //var url = endpoint.searchCompany;
    var rawOutput = await this.callRawWebsite('GET', url);

    var htmlObject = cheerio.load(rawOutput);
    var searchResults = htmlObject('section.content_ty3');
    var results = [];
    for(var i = 0; searchResults.length > i; i++) {
      var link = cheerio(searchResults[i]).find(".us_titb_l3 a").attr('href');
      var resultName = cheerio(searchResults[i]).find(".us_titb_l3 a").text().trim();
      var linkParts = link.split('/');
      var link = '/companies/' + linkParts[2] + '/landing';
      var name = resultName;
      results.push({name: he.decode(name), link: he.decode(link)});
    }
    return results;
  }

  this.getCompanyDetails = async function(companyDetailUrl) {
    //var rawOutput = await request({method: 'GET', url: url});
    var rawOutput = await this.callRawWebsite('GET', companyDetailUrl);
    var htmlObject = cheerio.load(rawOutput);

    var companyDetail = {};
    companyDetail.logo = he.decode(htmlObject('div.company_logo_box > a.thumb_wrap > span.img_wrap > img').attr('src'));
    if(companyDetail.logo.match(/default/g)) companyDetail.logo = '';

    var companyDetailItems = htmlObject('dl.info_item_more');
    for(var i = 0; companyDetailItems.length > i; i++) {
      var item = cheerio(companyDetailItems[i]);
      if(item.find('dt').text().trim() == '소개') {
        companyDetail.description = he.decode(item.find('dd').text().trim());
        break;
      }
    }
    return companyDetail;
  }
}

// Public
module.exports = Jobplanet;
