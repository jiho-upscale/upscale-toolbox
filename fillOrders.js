'use strict';

const utils = require('./utils.js');
const pool = require('./dbConfig.js');
const jackPool = require('./jackDbConfig.js');


var members = [];

var run = async function() {
  let sql = `select
            	c.*,
            	s.seq as stock_seq,
            	max(so.order_at) as lastOrder
            from companies as c
            join stocks as s
            on c.seq = s.company_seq
            left join stock_orders as so
            on s.seq = so.stock_seq
            group by c.seq`;
  let [companyResult, companyError] = await pool.query(sql);

  for(var i = 0; companyResult.length > i; i++) {
    var company = companyResult[i];
    var companyBaseName = utils.getCompanyBaseName(company.name);
    var lastOrder = company.lastOrder;
    if(lastOrder == null) lastOrder = '1970-01-01 00:00:00'
    console.log('Start filling orders for ' + companyBaseName);

    let sql = `select
              	*,
              	'ASK' as type
              from ask
              WHERE (trim(title) = ? OR trim(title) REGEXP concat('^', ?,'\\((.+)\\)$'))
              AND inserted > ?
              union
              select
              	*,
              	'BID' as type
              from bid
              WHERE (trim(title) = ? OR trim(title) REGEXP concat('^', ?,'\\((.+)\\)$'))
              AND inserted > ?`;
    let [orderResult, orderError] = await jackPool.query(sql, [companyBaseName, companyBaseName, lastOrder, companyBaseName, companyBaseName, lastOrder]);
    console.log('Retrieved ' + orderResult.length + ' orders for ' + companyBaseName);
    var insertedUsers = 0;
    var insertedOrders = 0;
    for(var x = 0; orderResult.length > x; x++) {
      let order = orderResult[x];
      let tel = order.tel.replace(/\D/g,'');
      let userId;

      let sql = "select ud.*, u.virtual_flag from user_details as ud join users as u on ud.user_seq = u.seq where ud.phone_number = ?";
      let [userResult, error] = await pool.query(sql, [tel]);
      var isVirtualUser;
      if(userResult.length == 0) {
        let email = tel + '@none.none';
        let password = '';
        let nickname = tel.substr(tel.length - 4);

        let sql = "insert into users (email, password, nickname, virtual_flag, leave_flag) values (?,?,?,?,?)";
        let [insertUserResult, insertUserError] = await pool.query(sql, [email, password, nickname, 1, 0]);
        userId = insertUserResult.insertId;
        isVirtualUser = 1;

        sql = "insert into user_details (user_seq, phone_number) values (?,?)";
        let [insertUserDetailResult, insertUserDetailError] = await pool.query(sql, [userId, tel]);

        sql = "insert into user_subscribe_settings (user_seq, sms_flag, email_flag) values (?,?,?)";
        let [insertUserSettingsResult, insertUserSettingsError] = await pool.query(sql, [userId, 1, 0]);
        insertedUsers++;
      } else {
        userId = userResult[0].user_seq;
        isVirtualUser = userResult[0].virtual_flag;
      }

      if(isVirtualUser == 1) {
        let orderType;
        if(order.type == 'ASK') {
          orderType = 'BUY';
        } else if(order.type == 'BID') {
          orderType = 'SELL';
        }

        let sql = "select * from stock_orders where user_seq = ? and stock_seq = ? and order_type = ? and stock_type = ? and price = ? and volume = ? and order_at = ?";
        let [stockResult, error] = await pool.query(sql, [userId, company.stock_seq, orderType, 'COMMON', order.price, order.quantity, order.inserted]);
        if(stockResult.length == 0) {
          let sql = "insert into stock_orders (user_seq, stock_seq, order_type, stock_type, price, volume, order_at) values (?,?,?,?,?,?,?)";
          let [insertResult, error] = await pool.query(sql, [userId, company.stock_seq, orderType, 'COMMON', order.price, order.quantity, order.inserted]);
          insertedOrders++
        }
      }
    }
    sql = `update stocks set trade_status = 'LIVE'
              where trade_status != 'LIVE'
              and seq in (
                select stock_seq from stock_orders
              )`;
    let [updateStatusResult, error] = await pool.query(sql);
    console.log('Done filling orders for ' + companyBaseName + ' [' + insertedUsers + ' users, ' + insertedOrders + ' orders]');
  }
}

run().then(function() {
  console.log('done');
  pool.end();
  jackPool.end();
});
