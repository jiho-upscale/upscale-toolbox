'use strict';

const request = require('request-promise-native');
const utils = require('./utils.js');

var Toast = new function() {

  this.sendSms = async function(title, message, recipients) {
    var appKey = 'BkQfvEGGJopxs5TD';

    var messageLength = utils.getEucKrByte(message);
    var url;
    var payload;

    var recipientList = [];
    for(var i=0; recipients.length > i; i++) {
      recipientList.push({
        recipientNo: recipients[i],
        countryCode: '82'
      });
    }

    if(messageLength > 80) {
      url = 'https://api-sms.cloud.toast.com/sms/v2.2/appKeys/' + appKey + '/sender/mms';
      payload = {
        title: title,
        body: message,
        sendNo: '0262071479',
        recipientList: recipientList
      };
    } else {
      url = 'https://api-sms.cloud.toast.com/sms/v2.2/appKeys/' + appKey + '/sender/sms';
      payload = {
        body: message,
        sendNo: '0262071479',
        recipientList: recipientList
      };
    }

    var options = {
      method: 'POST',
      url: url,
      headers: {
        'Content-type': 'application/json',
      },
      json: payload
    };

    var result;
    try {
      result = await request(options);
    } catch(e) {
      console.log('error' + e);
    }
    return result;
  };
}
// Public
module.exports = Toast;
