'use strict';

var Nps = new function() {

  const request = require('request-promise-native');
  const cheerio = require("cheerio");
  const xmlParser = require('fast-xml-parser');
  const queryEncode = require("querystring").encode;
  const utils = require('./utils.js');
  const host = 'http://apis.data.go.kr';
  const retryLimit = 50;
  var retryCount = 0;

  const endpoint = {
    searchCompany: '/B552015/NpsBplcInfoInqireService/getBassInfoSearch',
    companyDetail: '/B552015/NpsBplcInfoInqireService/getDetailInfoSearch'
  }

  var apiKey = 'pn/DfRAuCQmfxEwJUzvaSm2ON0shlEdxfO3fCeGS28vjhX1EuyMYNIyIj9BOonma3hBn3y3+ABMSQVwPSjiqtg==';


  this.callApi = async function(method, endpoint, query) {
    if(query == null) query = {};
    query.serviceKey = apiKey;

    var options = {
      method: method.toUpperCase()
    };

    if(method.toUpperCase() == 'GET') {
      options.url = host + endpoint + '?' + queryEncode(query);
    } else if(method.toUpperCase() == 'POST') {
      options.url = host + endpoint;
      options.formData = query;
    }

    console.log('[nps] call ' + options.url);
    var result = await request(options);
    if(xmlParser.validate(result)) return xmlParser.parse(result, {});
    else return result;
  }

  this.searchCompany = async function(companyName, tid) {
    var params = {};
    params.wkpl_nm = companyName;
    params.numOfRows = 10;
    params.bzowr_rgst_no = tid.toString().substring(0, 6);

    var result = await this.callApi('GET', endpoint.searchCompany, params);
    if(!result.response) return null;

    var getMatch = [];
    if(result.response.body.items === Object(result.response.body.items) && result.response.body.totalCount > 0) {
      if(Array.isArray(result.response.body.items.item)) {
        getMatch = result.response.body.items.item.filter(function(company) {
          var matchBaseName = utils.getCompanyBaseName(utils.toASCII(company.wkplNm)).replace(/\s/g, '');
          var companyBaseName = utils.getCompanyBaseName(companyName).replace(/\s/g, '');
          return matchBaseName == companyBaseName;
        });
      } else {
        var company = result.response.body.items.item;
        var matchBaseName = utils.getCompanyBaseName(utils.toASCII(company.wkplNm)).replace(/\s/g, '');
        var companyBaseName = utils.getCompanyBaseName(companyName).replace(/\s/g, '');
        if(matchBaseName == companyBaseName) {
          getMatch = [company];
        }
      }
    }

    if(getMatch.length == 0 && retryLimit > retryCount) {
      retryCount++;
      if(companyName == utils.getCompanyFullName(companyName)) {
        return await this.searchCompany(utils.getCompanyBaseName(companyName), tid);
      } else if(companyName != utils.getCompanyBaseName(companyName)) {
        return await this.searchCompany(utils.getCompanyFullName(companyName), tid);
      } else {
        console.log('company not found on nps records ' + companyName);
        return null;
      }
    }

    getMatch.sort((a, b) => (a.dataCrtYm < b.dataCrtYm) ? 1 : -1)
    var latest = getMatch[0];
    return latest;
  }

  this.getCompanyDetails = async function(seq) {
    var companyDetails = {};
    var result = await this.callApi('GET', endpoint.companyDetail, {seq: seq});
    companyDetails.name = result.response.body.item.wkplNm;
    companyDetails.employeeCount = result.response.body.item.jnngpCnt;
    companyDetails.industry = result.response.body.item.vldtVlKrnNm;
    companyDetails.tid = result.response.body.item.bzowrRgstNo;
    companyDetails.foundingDate = result.response.body.item.adptDt;
    return companyDetails;
  }
}

// Public
module.exports = Nps;
