'use strict';

var Utils = new function() {

  const request = require('request-promise-native');
  const cheerio = require("cheerio");
  const nodeCache = require( "node-cache" );
  const fs = require("fs");
  const queryEncode = require("querystring").encode;
  const proxyRetryLimit = 5;
  var proxyRetries = 0;
  var previousProxy = null;
  var proxyList = null;
  var currentCache = null;
  const AWS = require('aws-sdk');
  const mime = require('mime-types');
  const url = require('url');
  const path = require('path');

  AWS.config.update({
    region: 'ap-northeast-2',
    accessKeyId: 'AKIAVVTFQ6LSU3X6XXPZ',
    secretAccessKey: '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'
  });
  const s3 = new AWS.S3({apiVersion: '2006-03-01'});

  const staffRange = [
    {min: 1, max: 10},
    {min: 11, max: 50},
    {min: 51, max: 100},
    {min: 101, max: 200},
    {min: 201, max: 500},
    {min: 501, max: 1000},
    {min: 1001, max: 2000},
    {min: 2001, max: 5000},
    {min: 5001, max: 0},
  ];

  this.getStaffRange = function(staffNumber) {
    for(var i = 0; staffRange.length > i; i++) {
      if((staffRange[i].min <= parseInt(staffNumber) && staffRange[i].max >= parseInt(staffNumber)) || staffRange[i].max == 0) {
        return staffRange[i];
      }
    }
  }

  var cache = function() {
    if(currentCache == null) {
      currentCache = new nodeCache();
    }
    return currentCache;
  }
  this.cache = cache;

  var getProxyFromFile = async function() {
    var content = await fs.readFileSync('./proxies.json', 'utf8');
    if(proxyList == null) proxyList = JSON.parse(content);
    if(proxyList != null) {
      var randomIndex = Math.floor(Math.random() * proxyList.length);

      var badProxies = cache().get('badProxies');
      if(badProxies == null) badProxies = [];

      while(badProxies.includes(proxyList[randomIndex]) || previousProxy == proxyList[randomIndex]) {
        console.log('bad proxy ' + proxyList[randomIndex]);
        randomIndex = Math.floor(Math.random() * proxyList.length);
      }

      previousProxy = proxyList[randomIndex];

      return {curl: proxyList[randomIndex]};
    } else {
      return null;
    }
  };
  this.getProxyFromFile = getProxyFromFile;

  function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    });
  }

  var getProxyFromApi = async function() {
    var param = {
      api_key: 'bf9e530c-3997-4c25-975f-b755b1deeec6',
      protocol: 'socks4,socks5',
      //country: 'KR,JP,SG,TW,VN,TH,PH,MY',
      supportsHttps: 'true',
      cookies: 'true',
      get: 'true',
      post: 'true',
      maxCheckPeriod: 14400,
      'user-agent': 'true'
    };
    var url = 'https://gimmeproxy.com/api/getProxy?' + queryEncode(param);
    //var url = 'https://api.getproxylist.com/proxy?lastTested=600&protocol=http&allowsCookies=1&country[]=KR&country[]=JP&country[]=SG&country[]=TW&country[]=VN&country[]=TH&country[]=PH&country[]=MY';

    var options = {
      method: 'GET',
      url: url,
      timeout: 3000,
      headers: {
        'Cache-Control': 'no-cache'
      }
    };

    try {
      var result = await request(options);
    } catch(e) {
      if(retryLimit < retryCount) {
        retryCount++;
        return await getProxy();
      } else {
        console.log('error' + e);
      }
    }

    var proxyResult = JSON.parse(result);

    var badProxies = cache().get('badProxies');
    if(badProxies == null) badProxies = [];
    if(badProxies.includes(proxyResult.curl) || previousProxy == proxyResult.curl) {
      console.log('bad proxy ' + proxyResult.curl);
      return await getProxyFromApi();
    } else {
      previousProxy = proxyResult.curl;
    }
    return proxyResult;
  }
  this.getProxyFromApi = getProxyFromApi;

  var getProxy = async function() {
    return this.getProxyFromApi();
    //return this.getProxyFromFile();
  };
  this.getProxy = getProxy;

  this.toASCII = function(chars) {
    var ascii = '';
    for(var i=0, l=chars.length; i<l; i++) {
      var c = chars[i].charCodeAt(0);

      // make sure we only convert half-full width char
      if (c >= 0xFF00 && c <= 0xFFEF) {
        c = 0xFF & (c + 0x20);
      }
      ascii += String.fromCharCode(c);
    }
    return ascii;
  }

  this.getCompanyFullName = function(companyName) {
    var companyFullName = null;
    var regex = /(.*)(\([주|유]\))(.*)/g;
    var companyNameBreakdown = regex.exec(companyName);
    if(companyNameBreakdown != null) {
      if(companyNameBreakdown[1].length == 0) {
        if(companyNameBreakdown[2] == '(주)') companyFullName = '주식회사 ' + companyNameBreakdown[3];
        else if(companyNameBreakdown[2] == '(유)') companyFullName = '유한회사 ' + companyNameBreakdown[3];
      } else {
        if(companyNameBreakdown[2] == '(주)') companyFullName = companyNameBreakdown[1] + ' 주식회사';
        else if(companyNameBreakdown[2] == '(유)') companyFullName = companyNameBreakdown[1] + ' 유한회사';
      }
    } else {
      companyFullName = companyName;
    }
    return companyFullName;
  }

  this.getCompanyBaseName = function(companyName) {
    var companyName = companyName.replace(/\([주|유]\)/g, "");
    companyName = companyName.replace(/(유한회사)|(주식회사)/g, "");
    return companyName;
  }

  this.uploadFile = async function(filePath, key) {
    let options = {
      url: filePath,
      encoding: null,
      resolveWithFullResponse: true
    };
    let result = request.get(options).then(function(data) {
      let result = new Promise(function(resolve, reject) {
        var extension = '.' + mime.extension(data.headers['content-type']);
        if(extension == '.false') extension = path.extname(url.parse(filePath).pathname);
        let base64data = Buffer.from(data.body, 'binary');
        let params = {
          Bucket: 'static.upscale.kr',
          Key: 'files/' + key + extension,
          Body: base64data
        };
        s3.upload(params, function(err, data) {
          if (err) reject(err);
          resolve(data);
        });
      });
      return Promise.resolve(result);
    }).catch(function(error) {
      throw error;
    });
    return Promise.resolve(result);
  };

  this.getEucKrByte = function(str) {
    return str.split('').map(s => s.charCodeAt(0)).reduce((prev, c) => (prev + ((c === 10) ? 2 : ((c >> 7) ? 2 : 1))), 0);
  }

  this.getShortUrl = async function(longUrl) {
    const bitlyKey = '0427564b2814c3057f6922f6d1dfa37f4286bc06';
    const bitlyGuid = 'Bj8s3xyz10u';

    var options = {
      method: 'POST',
      url: 'https://api-ssl.bitly.com/v4/shorten',
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + bitlyKey
      },
      json: {
      	"group_guid": bitlyGuid,
      	"domain": "bit.ly",
      	"long_url": longUrl
      }
    };

    try {
      var result = await request(options);
    } catch(e) {
      console.log('error' + e);
    }
    return result;
  }
}

// Public
module.exports = Utils;
