'use strict';

var Thevc = new function() {

  const request = require('request-promise-native');
  const socksAgent = require('socks-proxy-agent');
  const utils = require('./utils.js');
  const useragent = require('random-useragent');
  const cheerio = require("cheerio");
  const htmlparser2 = require('htmlparser2');
  const queryEncode = require("querystring").encode;
  const pool = require('./dbConfig.js');
  const he = require('he');
  const host = 'https://thevc.kr';
  const retryLimit = 50;
  var retryCount = 0;
  var currentProxy;

  const endpoint = {
    searchCompany: '/search'
  }

  var getProxy = async function() {
    try {
      var proxyInfo = await utils.getProxy();
      //var agent = new proxy(proxyInfo.curl);
      var agent = new socksAgent(proxyInfo.curl);
      currentProxy = proxyInfo.curl;
      console.log('[thevc] proxy is: ' + currentProxy);
      return agent;
    } catch(error) {
      console.log(error);
      return getProxy();
    }
  }

  var callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    console.log('[thevc] call ' + url);

    var headers = {
      'User-Agent': useragent.getRandom(function(data) {
        if(data.folder == '/Spiders - Search') return true;
        else return false;
      })
    };

    var options = {
      method: method.toUpperCase(),
      url: url,
      headers: headers,
      agent: await getProxy(),
      timeout: 3000,
      resolveWithFullResponse: true
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }

    var result = request(options).then(function(result) {
      if(result == null || typeof result == 'undefined') {
        throw result;
      } else {
        retryCount = 0;
        return result.body;
      }
    }).catch(function(error) {
      if(error.response && (error.response.statusCode == 404 || error.response.statusCode == 500)) {
        throw error;
      }

      var badProxies = utils.cache().get('badProxies');
      if(badProxies == null) badProxies = [];
      badProxies.push(currentProxy);
      utils.cache().set('badProxies', badProxies);

      if(retryLimit > retryCount) {
        retryCount++;
        console.log('retry thevc : ' + retryCount);
        return callRawWebsite(method, endpoint, query, body);
      } else {
        console.log(error);
        process.exit();
      }
    });
    return Promise.resolve(result);
  }
  this.callRawWebsite = callRawWebsite;

  this.search = async function(companyName) {
    var rawOutput = await callRawWebsite('GET', endpoint.searchCompany, {word: companyName}, null);
    var resultObject = JSON.parse(rawOutput);
    var results = resultObject.c;

    var searchResults = [];
    for(var i = 0; results.length > i; i++) {
      var searchResult = results[i];
      var name = searchResult.kor;
      var link = '/' + searchResult.name_url;
      if(utils.getCompanyBaseName(name) == utils.getCompanyBaseName(companyName)) {
        searchResults.push({name: name.trim(), link: link});
      }
    }

    return searchResults;
  }

  var getInvestor = async function(source) {
    var sql = "select * from investors where name_kr = ?";
    var [result, error] = await pool.query(sql, [source.name]);
    var investorData;
    if(result.length == 0) {
      investorData = callRawWebsite('GET', source.link, null, null).then(function(investorData) {
        var dom = htmlparser2.parseDOM(investorData);
        var investorHtmlObject = cheerio.load(dom);
        var name = investorHtmlObject('h1').text().trim();
        var logo = investorHtmlObject('img.logo_img').attr('src');
        var website = investorHtmlObject('div.contact_info > ul > li:last-child > span > a').attr('href');

        if(typeof name == 'undefined' || typeof logo == 'undefined' || typeof website == 'undefined') {
          throw 'catch error and retry ' + source.link;
        } else {
          var investor = {
            name: he.decode(name),
            logo: he.decode(logo),
            website: he.decode(website)
          };
          return Promise.resolve(investor);
        }
      }).catch(function(error) {
        console.log(error);
        return getInvestor(source);
      });
    } else {
      investorData = {
        name: result[0].name_kr,
        logo: result[0].logo_path,
        website: result[0].site_url
      };
    }

    return Promise.resolve(investorData);
  }

  var getCompanyDetails = async function(companyDetailUrl) {
    var companyDetails = {};
    var rawOutput = await callRawWebsite('GET', companyDetailUrl, null, null);
    var dom = htmlparser2.parseDOM(rawOutput);
    var htmlObject = cheerio.load(dom);

    var status = htmlObject('h2:contains(회사정보)').siblings('ul').find('li:contains(상태) > span').text().trim();
    var receivedInvestmentAmount = htmlObject('p.sum_invested').text().trim();
    if(receivedInvestmentAmount == '' || status != '비상장') return null;

    var result = {};
    result.logo = htmlObject('div.head > div.abst > img').attr('src');

    console.log('[thevc] start scraping news');
    var news = [];
    var newsItems = htmlObject('ul.media > li');

    try {
      for(var i = 0; newsItems.length > i; i++) {
        var newsItem = cheerio(newsItems[i]);
        var mediaName = newsItem.children('div').first().find('a').text().trim();
        var newsDate = newsItem.children('div').first().find('span').text().trim();
        var newsLink = newsItem.find('h3:first-child a').first().attr('href');
        var newsHeadline = newsItem.find('h3:first-child a').first().text().trim();

        if(typeof newsLink == 'undefined') {
          throw 'catch error and retry';
        }

        console.log('[thevc] news - ' + mediaName + ' ' + newsHeadline + ' ' + newsDate);
        console.log(he.decode(newsLink));
        news.push({
          link: he.decode(newsLink),
          headline: he.decode(newsHeadline),
          media: he.decode(mediaName),
          date: he.decode(newsDate)
        });
      }

      if(news.length > 0 && news[0].headline.trim().length == 0) {
        throw 'catch error and retry';
      }
    } catch(error) {
      if(retryLimit > retryCount) {
        retryCount++;
        console.log('retry ' + retryCount);
        return await getCompanyDetails(companyDetailUrl);
      }
    }
    retryCount = 0;

    result.news = news;
    console.log('[thevc] completed scraping news');

    var regex = /graph_invested_dash\((.*)\)/g;
    var investmentRawData = regex.exec(rawOutput);
    var investmentRoundData = JSON.parse(investmentRawData[1].replace(/'/g, '"'));

    console.log('[thevc] start scraping investment rounds');
    var investmentRounds = [];
    for(var i = 0; investmentRoundData.length > i; i++) {
      var round = {};
      round.round = he.decode(investmentRoundData[i].round);
      round.amount = investmentRoundData[i].amount;
      round.date = he.decode(investmentRoundData[i].date);

      console.log('[thevc] investment round - ' + round.round + ' ' + round.amount + ' ' + round.date);

      var investmentRoundSelector = 'tl_' + investmentRoundData[i].id;
      var investmentRoundObjects = htmlObject('ul.investors_list > li.' + investmentRoundSelector);

      round.investors = [];
      var investorSources = [];
      await investmentRoundObjects.each(async function(i, elm) {
        var logo = cheerio(this).find('img').attr('src');
        var divs = cheerio(this).find('div');
        var investorName = cheerio(divs[1]).find('a').text();

        if(investorName.trim().length > 0) {
          console.log('   investor: ' + investorName);
          var investorLink = cheerio(divs[1]).find('a').attr('href');
          investorSources.push({name: he.decode(investorName), link: he.decode(investorLink)});
        }
      });

      var investorCalls = [];
      for(var x = 0; investorSources.length > x; x++) {
        var investorCall = getInvestor(investorSources[x]).then(function(investor) {
          round.investors.push(investor);
        });
        investorCalls.push(investorCall);
      }
      await Promise.all(investorCalls);
      investmentRounds.push(round);
    }
    result.investmentRounds = investmentRounds;
    console.log('[thevc] completed scraping investment rounds');

    var products = [];
    var tags = [];
    var productObjects = htmlObject('div.product_box');
    for(var i = 0; productObjects.length > i; i++) {
      let productObject = cheerio(productObjects[i]);
      var productName = productObject.find('a > span.product_name').first().text().trim();
      var productLink = productObject.find('div.img_wrapper > a').attr('href');
      var productLogo = productObject.find('div.img_wrapper > a > img').attr('src');
      var productDescription = productObject.find('div.info_wrapper > span').not('.product_name').text().trim();
      products.push({
        name: he.decode(productName),
        logo: he.decode(productLogo),
        header: '',
        website: '',
        description: he.decode(productDescription)
      });
      var productTags = productObject.find('div.info_wrapper > p > span');
      for(var y = 0; productTags.length > y; y++) {
        var tag = cheerio(productTags[y]);
        tags.push(tag.text().trim());
      }
    }

    result.products = products;
    result.website = htmlObject('div.contact_info > ul > li:last-child > span > a').text();
    result.address = htmlObject('div.map_wrap > div a').text();
    result.tags = Array.from(new Set(tags));
    return result;
  }
  this.getCompanyDetails = getCompanyDetails;
}

// Public
module.exports = Thevc;
