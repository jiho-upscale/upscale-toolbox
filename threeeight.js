'use strict';

var threeeight = new function() {

  const request = require('request-promise-native');
  const socksAgent = require('socks-proxy-agent');
  const useragent = require('random-useragent');
  const cheerio = require("cheerio");
  const charset = require("charset");
  const iconvEncode = require('iconv-urlencode');
  const iconvLite  = require('iconv-lite');
  const utils = require('./utils.js');
  const queryEncode = require("querystring").encode;
  const he = require('he');
  const host = 'http://www.38.co.kr';
  const retryLimit = 100;
  var retryCount = 0;
  var currentProxy;

  const endpoint = {
    searchCompany: '/html/forum/com_list/',
    companyBoard: '/html/forum/board/'
  }

  var getProxy = async function() {
    try {
      var proxyInfo = await utils.getProxy();
      //var agent = new proxy(proxyInfo.curl);
      var agent = new socksAgent(proxyInfo.curl);
      currentProxy = proxyInfo.curl;
      console.log('[threeeight] proxy is: ' + currentProxy);
      return agent;
    } catch(error) {
      console.log(error);
      return getProxy();
    }
  }

  var callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    console.log('[threeeight] call ' + url);

    var headers = {
      'User-Agent': useragent.getRandom(function(data) {
        if(data.folder == '/Browsers - Mac') return true;
        else return false;
      })
    };

    var options = {
      method: method.toUpperCase(),
      url: url,
      headers: headers,
      //agent: await getProxy(),
      timeout: 3000,
      encoding: null,
      resolveWithFullResponse: true
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }

    var result = request(options).then(function(result) {
      const siteEnc = charset(result.headers, result.body)
      const decoded = iconvLite.decode(result.body, siteEnc || 'utf8');
      return decoded;
    }).catch(function(error) {
      console.log(error);
      if(error.response && (error.response.statusCode == 404 || error.response.statusCode == 500)) {
        throw error;
      }

      var badProxies = utils.cache().get('badProxies');
      if(badProxies == null) badProxies = [];
      badProxies.push(currentProxy);
      utils.cache().set('badProxies', badProxies);

      if(retryLimit > retryCount) {
        retryCount++;
        console.log('retry threeeight : ' + retryCount);
        return callRawWebsite(method, endpoint, query, body);
      } else {
        throw error;
      }
    });

    return Promise.resolve(result);
  }
  this.callRawWebsite = callRawWebsite;

  this.getCompanyDetailsByName = async function(companyName, niceCode) {
    var encodedCompanyName = iconvLite.encode(companyName, 'euc-kr');
    var rawOutput = await this.callRawWebsite('POST', endpoint.searchCompany, null, {keyword: encodedCompanyName});
    var htmlObject = cheerio.load(rawOutput);
    var rows = htmlObject('table[summary*="기업정보 수정현황"] > tbody > tr');
    var results = {};
    for(var i = 0; rows.length > i; i++) {
      var row = rows[i];
      if(cheerio(row).attr('bgcolor')) {
        let code = cheerio(cheerio(row).find('td')[1]).text().trim();
        let name = cheerio(cheerio(row).find('td')[2]).text().trim();
        let capInfo = await this.getCompanyDetailsByCode(code);
        if(capInfo.niceCode && capInfo.niceCode == niceCode) {
          results = capInfo;
          results.name = he.decode(name);
          break;
        } else if(companyName == name) {
          results = capInfo;
          results.name = he.decode(name);
          break;
        }
      }
    }
    return results;
  }

  this.getCompanyDetailsByCode = async function(code) {
    var rawOutput = await callRawWebsite('GET', endpoint.companyBoard, { code: code });
    var htmlObject = cheerio.load(rawOutput);

    var rows = htmlObject('table[summary*="기업개요"] > tbody > tr');
    var companyBaseName = cheerio(cheerio(rows[3]).find('td')[1]).text().trim();
    var ceo = cheerio(cheerio(rows[4]).find('td')[1]).text().trim().replace(/\s/g, '');
    var foundingDate = cheerio(cheerio(rows[4]).find('td')[3]).text().trim().replace(/\s/g, '');
    var industry = cheerio(cheerio(rows[5]).find('td')[1]).text().trim().replace(/\s/g, '');
    var phoneNumber = cheerio(cheerio(rows[6]).find('td')[1]).text().trim().replace(/\s/g, '');
    var website = cheerio(cheerio(rows[7]).find('td')[1]).text().trim().replace(/\s/g, '');
    var address = cheerio(cheerio(rows[8]).find('td')[1]).text().trim();
    var totalShares = cheerio(cheerio(rows[9]).find('td')[1]).text().trim().replace(/\D/g,'').replace(/\s/g, '');
    var faceValue = cheerio(cheerio(rows[9]).find('td')[3]).text().trim().replace(/\D/g,'').replace(/\s/g, '');
    var capitalization = cheerio(cheerio(rows[10]).find('td')[1]).text().trim().replace(/\s/g, '');
    var unit = capitalization.slice(-2);
    var amount = capitalization.substring(0, capitalization.length-2);
    if(unit == '억원') {
      var capitalizationAmount = amount*100000000;
    }

    var niceLink = htmlObject('table[summary*="기업개요"]').next('table').find('a').attr('href');
    var regex = /(kiscode=)(.+)[&]/g;
    var match = regex.exec(niceLink);

    var result = {
      companyBaseName: he.decode(companyBaseName),
      ceoName: he.decode(ceo),
      foundingDate: he.decode(foundingDate),
      phoneNumber: he.decode(phoneNumber),
      website: he.decode(website),
      address: he.decode(address),
      totalShares: he.decode(totalShares),
      faceValue: he.decode(faceValue),
      capitalization: capitalizationAmount
    };
    if(match) result.niceCode = he.decode(match[2]);
    return result;
  }
}

// Public
module.exports = threeeight;
