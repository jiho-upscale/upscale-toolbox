'use strict';

const dart = require('./dart.js');
const nps = require('./nps.js');
const jobplanet = require('./jobplanet.js');
const rocketpunch = require('./rocketpunch.js');
const thevc = require('./thevc.js');
const nice = require('./nice.js');
const saramin = require('./saramin.js');
const threeeight = require('./threeeight.js');
const utils = require('./utils.js');
const pool = require('./dbConfig.js');
const jackPool = require('./jackDbConfig.js');
const dbOperations = require('./dbOperations.js');
const inquirer = require('inquirer');

const getCompanies = async function() {
  var companies = [
    ['쿠팡(주)'],
    ['(주)컬리', '더파머스', '더파머스, 마켓컬리'],
    ['와디즈플랫폼(주)', '와디즈'],
    ['(주)브레이브모바일', '숨고'],
    ['(주)야놀자'],
    ['(주)직방'],
    ['(주)우아한형제들'],
    ['(유)미소'],
    ['두나무(주)'],
    ['(주)비티씨코리아닷컴'],
    ['(주)크래프톤'],
    ['(주)비바리퍼블리카'],
    ['(주)스테이션3'],
    ['(주)레이니스트'],
    ['크로키닷컴(주)'],
    ['스마트스터디(주)'],
    ['(주)위드이노베이션'],
    ['(주)마이리얼트립'],
    ['(주)빅히트엔터테인먼트'],
    ['(주)옐로모바일'],
    ['(주)미미박스'],
    ['에어프레미아(주)'],
    ['(주)쏘카'],
    ['(주)네시삼십삼분'],
    ['(주)디앤디파마텍'],
    ['(주)티몬', '티켓몬스터'],
    ['라인게임즈(주)'],
    ['베스핀글로벌(주)'],
    ['(주)데일리금융그룹'],
    ['(주)위메프'],
    ['(주)메쉬코리아'],
    ['패스트파이브(주)'],
    ['엘앤피코스메틱(주)'],
    ['(주)써머스플랫폼'],
    ['(주)아발론교육'],
    ['(주)넵튠'],
    ['(주)피플펀드컴퍼니'],
    ['(주)레진엔터테인먼트'],
    ['(주)엔드림'],
    ['(주)베이글코드'],
    ['(주)뉴라클사이언스'],
    ['(주)타이드스퀘어'],
    ['(주)피투피시스템즈'],
    ['(주)밸런스히어로'],
    ['오름테라퓨틱(주)'],
    //['(주)트리플'],
    ['(주)엑소코바이오'],
    ['(주)얍컴퍼니'],
    ['(주)샌드박스네트워크'],
    ['(주)프렌즈게임즈'],
    ['(주)그린카'],
    ['(주)메이크어스'],
    ['(주)중고나라'],
    ['리디(주)'],
    ['(주)바로고'],
    ['(주)블랭크코퍼레이션'],
    ['(주)마인즈랩'],
    ['(주)글로벌네트웍스'],
    ['(주)마이뮤직테이스트'],
    ['(주)문피아'],
    ['(주)비비비'],
    ['(주)와그트래블'],
    ['(주)렌딧'],
    ['(주)스틸에잇'],
    ['(주)마이쿤'],
    ['(주)에잇퍼센트'],
    ['(주)풀러스'],
    ['(주)힐세리온'],
    ['(주)원티드랩'],
    ['(주)어니스트펀드'],
    ['(주)왓챠'],
    ['(주)루닛'],
    ['(주)아이엔지스토리'],
    ['(주)스파크플러스'],
    ['(주)네오펙트'],
    ['집닥(주)'],
    ['(주)터크앤컴퍼니'],
    ['파킹클라우드(주)'],
    ['(주)인코어드테크놀로지스'],
    ['(주)이노테라피'],
    ['(주)엔비티'],
    ['(주)허니비즈'],
    ['(주)스탠다임'],
    ['(주)지놈앤컴퍼니'],
    ['(주)마이셀럽스'],
    ['(주)딜리셔스'],
    ['(주)버즈빌'],
    ['(주)백패커'],
    ['(주)투게더앱스'],
    ['(주)하우투메리'],
    ['(주)뷰노'],
    ['(주)고바이오랩'],
    ['(주)링크샵스'],
    ['(주)비브로스'],
    ['(주)클래스팅'],
    ['(주)레모넥스'],
    ['(주)크몽'],
    ['(주)유비케어'],
    ['(주)에스티유니타스'],
    ['식신(주)'],
    ['(주)신테카바이오'],
    ['(주)큐젠바이오텍'],
    ['(주)하임바이오'],
    ['아퓨어스(주)', '메디키네틱스'],
    ['(주)엑스엘게임즈'],
    ['(주)비씨켐'],
    ['브릿지바이오테라퓨틱스(주)', '브릿지바이오'],
    ['(주)유베이스'],
    ['(주)메디오젠'],
    ['포스크리에이티브파티(주)'],
    ['(주)테크로스'],
    ['쉬프트정보통신(주)'],
    ['(주)폴루스'],
    ['(주)오콘'],
    ['(주)휴림바이오셀'],
    ['(주)지란지교'],
    ['리딩투자증권(주)'],
    ['바이젠셀(주)'],
    ['퓨쳐메디신(주)'],
    ['웰마커바이오(주)'],
    ['파낙스이텍(주)'],
    ['(주)캐리소프트']
  ];

/*
  var companies = [
    '(주)에이프로젠',
    '지스마트(주)',
    '바이오플러스(주)',
    '(주)필로시스',
    '(주)신테카바이오',
    '(주)큐젠바이오텍',
    '(주)진켐',
    '(주)티맥스소프트',
    '아퓨어스(주)',
    '(주)덴티스',
    '(주)지니틱스',
    '(주)엑스엘게임즈',
    '(주)네오바이오텍',
    '(주)제테마',
    '(주)메드팩토',
    '(주)하임바이오',
    '한국코러스(주)',
    '(주)노바셀테크놀로지',
    '(주)유베이스',
    '(주)아크로스',
    '(주)와이엠텍',
    '(주)메디오젠',
    '엔쓰리엔(주)',
    '(주)비씨켐',
    '(주)가온셀',
    '(주)피플바이오',
    '(주)제넨셀',
    '(주)테크로스',
    '(주)노보셀바이오',
    '(주)에스아이플렉스',
    '(주)싸이버로지텍',
    '넷마블네오(주)',
    '(주)비비비',
    '브릿지바이오(주)',
    '포스크리에이티브파티(주)',
    '한국씨엔티(주)',
    '(주)에프엑스기어',
    '(주)엔지노믹스',
    '대우정보시스템(주)',
    '(주)와이즈넛',
    '(주)피피아이',
    '(주)솔루엠',
    '(주)팡스카이',
    '(주)다음에너지',
    '쉬프트정보통신(주)',
    '(주)휴림바이오셀',
    '디에스글로벌(주)',
    '(주)오콘',
    '(주)지란지교',
    '(주))티맥스데이터',
    '(주)에빅스젠',
    '리딩투자증권(주)',
    '레이트론(주)',
    '(주)선재하이테크',
    '(주)보타메디',
    '웰마커바이오(주)',
    '케어캠프(주)',
    '(주)지엠피바이오',
    '파낙스이텍(주)',
    '(주)천랩',
    '대보정보통신(주)',
    '(주)에이피알',
    '(주)다원메닥스',
    '보로노이(주)',
    '웅진식품(주)',
    '바이젠셀(주)',
    '(주)스마트로',
    '메콕스큐어메드(주)',
    '그래핀스퀘어(주)',
    '(주)폴루스',
    '에스케이텔레시스(주)',
    '퓨쳐메디신(주)',
    '(주)라이트팜텍',
    '(주)와이바이오로직스',
    '(주)로킷헬스케어',
    '(주)메인라인',
    '(주)케이지에듀원',
    '(주)스타네크',
    '(주)유라클',
    '(주)이뮨메드',
    '(주)디와이엠솔루션',
    '(주)쎄니팡',
    '(주)하이코어',
    '미라셀(주)',
    '(주)오스테오시스',
    '(주)이노시뮬레이션',
    '(주)아이지에이웍스',
    '프레스티지바이오제약(주)',
    '(주)알티베이스',
    '(주)나노브릭',
    '(주)세경하이테크',
    '(주)펀진',
    '(주)나우코스',
    '에스씨엠생명과학(주)',
    '(주)비센바이오',
    '(주)제4기한국',
    '(주)제이미크론',
    '(주)테라리소스',
    '오씨아이스페셜티(주)',
    '(주)지플러스생명과학',
    '(주)곰앤컴퍼니',
    '(주)실크로드시앤티',
    '제너럴바이오(주)',
    '프리닉스(주)',
    '(주)카카오게임즈',
    '(주)메가젠임플란트',
    '(주)한국클래드텍',
    '(주)바이오이즈',
    '(주)메디셀',
    '(주)프로셀테라퓨틱스',
    '(주)카카오페이지',
    '바이오제멕스(주)',
    '(주)나래나노텍',
    '(주)천호엔케어',
    '아가월드(주)',
    '(주)엘지씨엔에스',
    '(주)시지바이오',
    '세틀뱅크(주)',
    '올리패스(주)',
    '드로젠(주)',
    '바이오스펙트럼(주)',
    '(주)제이시스메디칼',
    '(주)오코스모스',
    '(주)이지메디컴',
    '(주)압타머사이언스',
    '(주)인터컨스텍',
    '(주)뷰텔',
    '씨티씨코리아(주)',
    '(주)알바이오',
  ];
*/
  return companies;
}

const run = async function() {
  var companies = getCompanies();
  for(var i = 0; companies.length > i; i++) {
    var companyNames = companies[i];
    var startup = {};

    var sql = "select * from companies where name = ?";
    var [result, error] = await pool.query(sql, [companyNames[0]]);
    if(result.length > 0) {
      console.log('Company already exists in databse ' + companyNames[0]);
      continue;
    }

    var niceCode;
    var niceResult = await nice.search(companyNames[0]);
    if(niceResult.length == 1) {
      niceCode = niceResult[0].code;
    } else if(niceResult.length > 1) {
      var promptChoices = [];
      for(var j = 0; niceResult.length > j; j++) {
        promptChoices.push({
          name: niceResult[j].name + ' | ' + niceResult[j].industry + ' | ' + niceResult[j].address,
          value: j
        });
      }
      var result = await inquirer.prompt({
        type: 'list',
        name: 'answer',
        message: 'More than 1 company exists with the same name, choose one:',
        choices: promptChoices
      });
      niceCode = niceResult[result.answer].code;
    } else if(niceResult.length == 0) {
      console.log('Company doesn\'t exist ' + companyNames[0]);
      continue;
    }

    var niceCompanyDetails = await nice.getCompanyDetails(niceCode);
    if(niceCompanyDetails.name.length == 0) {
      console.log('Company doesn\'t exist ' + companyNames[0]);
      continue;
    }

    startup.name = companyNames[0];
    startup.ceoName = niceCompanyDetails.ceoName;
    startup.address = niceCompanyDetails.address;
    startup.businessType = niceCompanyDetails.industry;
    startup.foundingDate = niceCompanyDetails.foundationDate.replace(/\D/g,'');
    startup.phoneNumber = '';
    startup.logo = '';
    startup.investments = [];
    startup.website = '';
    startup.about = '';
    startup.products = [];
    startup.baseName = utils.getCompanyBaseName(companyNames[0]);
    startup.news = [];
    startup.staffMin = niceCompanyDetails.staffMin;
    startup.staffMax = niceCompanyDetails.staffMax;
    startup.businessNo = niceCompanyDetails.tid.replace(/-/g,'');
    startup.dartNotices = [];
    startup.lines = [];
    var tid = niceCompanyDetails.tid.replace(/\D/g,'');

    startup.totalShares = 0;
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      var capDetails = await threeeight.getCompanyDetailsByName(thisCompanyName, niceCode);
      if(capDetails.totalShares) startup.totalShares = capDetails.totalShares;
    }

    var companyCode = await dart.search(startup.baseName);
    if(companyCode != null) {
      var dartInfo = await dart.getCompanyInfo(companyCode);
      startup.ceoName = dartInfo.ceo_nm;
      startup.foundingDate = dartInfo.est_dt;
      startup.businessNo = dartInfo.bsn_no;
      startup.address = dartInfo.adr;
      startup.website = dartInfo.hm_url;
      startup.phoneNumber = dartInfo.phn_no;
      startup.dartNotices = await dart.getCompanyNotice(companyCode);
    }

    var npsSearchResults = await nps.searchCompany(companyNames[0], tid);
    if(npsSearchResults) {
      var npsCompanyDetails = await nps.getCompanyDetails(npsSearchResults.seq);
      var range = utils.getStaffRange(npsCompanyDetails.employeeCount);
      startup.staffMin = range.min;
      startup.staffMax = range.max;
      startup.businessNo = npsCompanyDetails.tid;
      if(startup.foundingDate.length != 8) startup.foundingDate = npsCompanyDetails.foundingDate.replace(/\D/g,'');
    }

    var productNames = [];
    var theVcSearchResults = [];
    var theVcSearchResult = [];
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      theVcSearchResults = await thevc.search(thisCompanyName);
      if(theVcSearchResults.length > 0) {
        theVcSearchResult = theVcSearchResults.filter(function(company) {
          return company.name.replace(/\s/g, '') == thisCompanyName.replace(/\s/g, '');
        });
        break;
      }
    }

    if(theVcSearchResult.length > 0) {
      var theVcCompanyDetails = await thevc.getCompanyDetails(theVcSearchResult[0].link);
      if(theVcCompanyDetails != null) {
        if(theVcCompanyDetails.logo) startup.logo = theVcCompanyDetails.logo;
        if(theVcCompanyDetails.investmentRounds) startup.investments = theVcCompanyDetails.investmentRounds;
        if(theVcCompanyDetails.website) startup.website = theVcCompanyDetails.website;
        if(theVcCompanyDetails.address) startup.address = theVcCompanyDetails.address;
        if(theVcCompanyDetails.news) startup.news = theVcCompanyDetails.news;
        if(theVcCompanyDetails.tags) startup.lines = theVcCompanyDetails.tags;

        for(var x = 0; theVcCompanyDetails.products.length > x; x++) {
          productNames.push(theVcCompanyDetails.products[x].name);
        }
        if(theVcCompanyDetails.products) startup.products = theVcCompanyDetails.products;
      }
    }

    var jobplanetSearchResults = [];
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      jobplanetSearchResults = await jobplanet.search(thisCompanyName);
      if(jobplanetSearchResults.length > 0) break;
    }
    if(jobplanetSearchResults.length > 0) {
      var jobplanetSearchResult = jobplanetSearchResults.filter(function(company) {
        var baseName = utils.getCompanyBaseName(company.name);
        return baseName.replace(/\s/g, '') == startup.baseName.replace(/\s/g, '');
      });

      if(jobplanetSearchResult.length > 0) {
        var jobplanetCompanyDetails = await jobplanet.getCompanyDetails(jobplanetSearchResult[0].link);
        if(startup.logo.length == 0) startup.logo = jobplanetCompanyDetails.logo;
        if(jobplanetCompanyDetails.description != null && jobplanetCompanyDetails.description.length > 5) {
          startup.about = jobplanetCompanyDetails.description;
        }
      }
    }

    var rocketpunchLink = null;
    var rocketpunchCompanyName;
    for(var j = 0; companyNames.length > j; j++) {
      let thisCompanyName = utils.getCompanyBaseName(companyNames[j]);
      rocketpunchLink = await rocketpunch.findCompanyDetailPage(thisCompanyName, productNames);
      if(rocketpunchLink != null) {
        rocketpunchCompanyName = thisCompanyName;
        break;
      }
    }

    if(rocketpunchLink != null) {
      var rocketPunchCompanyDetails = await rocketpunch.getCompanyDetails(rocketpunchLink);

      if(rocketPunchCompanyDetails.products.length > 0) {
        var deDuplicatedProducts = [];
        if(startup.products.length > 0) {
          for(var y = 0; startup.products.length > y; y++) {
            var productName = startup.products[y].name;
            var matchProduct = rocketPunchCompanyDetails.products.filter(function(object) {
              return object.name.replace(/\s/g,'') == productName.replace(/\s/g,'');
            });
            if(matchProduct.length == 0) {
              deDuplicatedProducts.push(startup.products[y]);
            }
          }
        }
        startup.products = deDuplicatedProducts.concat(rocketPunchCompanyDetails.products);
      }

      if(rocketPunchCompanyDetails.news.length > 0) {
        var deDuplicatedNews = [];
        if(startup.news.length > 0) {
          for(var y = 0; startup.news.length > y; y++) {
            var newsLink = startup.news[y].link;
            var matchNews = rocketPunchCompanyDetails.news.filter(function(object) {
              return object.link.trim() == newsLink.trim();
            });
            if(matchNews.length == 0) {
              deDuplicatedNews.push(startup.news[y]);
            }
          }
        }
        startup.news = deDuplicatedNews.concat(rocketPunchCompanyDetails.news);
      }
      startup.news.sort(function(a, b) {
        return b.date.replace(/-/g, '') - a.date.replace(/-/g, '');
      });

      if(rocketPunchCompanyDetails.tags.length > 0) startup.lines = Array.from(new Set(startup.lines.concat(rocketPunchCompanyDetails.tags)));
      if(!startup.logo) startup.logo = rocketPunchCompanyDetails.logo;
      if(rocketPunchCompanyDetails.description != null && startup.about.length < rocketPunchCompanyDetails.description.length) startup.about = rocketPunchCompanyDetails.description;
      if(rocketPunchCompanyDetails.phoneNumber) startup.phoneNumber = rocketPunchCompanyDetails.phoneNumber;

      for(var x = 0; startup.products.length > x; x++) {
        var product = startup.products[x];
        var matchProduct = [];
        if(theVcCompanyDetails) {
          matchProduct = theVcCompanyDetails.products.filter(function(object) {
            return object.name.replace(/\s/g,'') == product.name.replace(/\s/g,'');
          });
        }
        if(matchProduct.length > 0) {
          startup.products[x].logo = matchProduct[0].logo;
        } else {
          startup.products[x].logo = '';
        }
      }
    }

    console.log(startup);
    await dbOperations.insertCompany(startup);
  }
};

run().then(function() {
  console.log('done');
  pool.end();
});
