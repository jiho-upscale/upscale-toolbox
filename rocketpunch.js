'use strict';

var Rocketpunch = new function() {

  const request = require('request-promise-native');
  const proxy = require('https-proxy-agent');
  const socksAgent = require('socks-proxy-agent');
  const utils = require('./utils.js');
  const useragent = require('random-useragent');
  const cheerio = require("cheerio");
  const queryEncode = require("querystring").encode;
  const he = require('he');
  const host = 'https://www.rocketpunch.com';
  const retryLimit = 50;
  var retryCount = 0;

  const endpoint = {
    login: '/api/users/login',
    searchCompany: '/api/companies/template'
  }

  const userEmail = 'hello@upscale.kr'
  const userPassword = 'rMLCkBw7mb*dN2G-'

  var authCookies = null;
  var agent = null;
  var currentProxy;

  var callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    console.log('[rocketpunch] call ' + url);

    var headers = {
      'User-Agent': useragent.getRandom(function(data) {
        if(data.folder == '/Browsers - Mac') return true;
        else return false;
      })
    };
    if(authCookies != null) headers.Cookie = authCookies;


    if(agent == null) {
      var proxyInfo = await utils.getProxy();
      currentProxy = proxyInfo.curl;
      console.log('[rocketpunch] proxy is: ' + currentProxy);
      //agent = new proxy(proxyInfo.curl);
      agent = new socksAgent(currentProxy);
    }

    var options = {
      method: method.toUpperCase(),
      url: url,
      headers: headers,
      agent: agent,
      timeout: 3000,
      resolveWithFullResponse: true
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }

    var result = request(options).then(function(result) {
      if(result == null || typeof result == 'undefined') {
        throw result;
      } else {
        retryCount = 0;
        return result;
      }
    }).catch(function(error) {
      if(error.response && (error.response.statusCode == 404 || error.response.statusCode == 500)) {
        throw error;
      }
      var badProxies = utils.cache().get('badProxies');
      if(badProxies == null) badProxies = [];
      badProxies.push(currentProxy);
      utils.cache().set('badProxies', badProxies);

      if(retryLimit > retryCount) {
        retryCount++;
        agent = null // reset proxy
        authCookies = null; // reset auth
        console.log('retry rocketpunch : ' + retryCount);
        return callRawWebsite(method, endpoint, query, body);
      } else {
        console.log(error);
        process.exit();
      }
    });
    return Promise.resolve(result);
  }
  this.callRawWebsite = callRawWebsite;

  var login = async function() {
    var loginCookies = null;
    var loginResult = await callRawWebsite('POST', endpoint.login, null, {email: userEmail, password: userPassword});
    authCookies = loginResult.headers['set-cookie'];
    return authCookies;
  }
  this.login = login;

  this.findCompanyDetailPage = async function(companyName, productNames) {
    var results = await this.search(companyName);
    for(var i = 0; results.length > i; i++) {
      if(companyName.trim().replace(/\s/g,'') == results[i].name.trim().replace(/\s/g,'')) {
        return results[i].link;
      } else if(productNames.includes(results[i].name)) {
        return results[i].link;
      }
    }

    if(productNames.length > 0) {
      for(var i = 0; productNames.length > i; i++) {
        var productName = productNames[i];
        var results = await this.search(productName);
        for(var x = 0; results.length > x; x++) {
          if(results[x].name.trim().replace(/\s/g,'') == companyName.trim().replace(/\s/g,'') || results[x].name.trim().replace(/\s/g,'') == productName.trim().replace(/\s/g,'')) {
            return results[x].link;
          }
        }
      }
    }
    return null;
  }

  this.search = async function(companyName) {
    var rawOutput = await this.callRawWebsite('GET', endpoint.searchCompany, {keywords: companyName}, null);
    var resultObject = JSON.parse(rawOutput.body);
    var htmlObject = cheerio.load(resultObject.data.template);
    var searchResults = htmlObject('div#company-list .item');

    var results = [];
    await searchResults.each(function(i, elm) {
      var resultName = cheerio(this).find('h4.header strong').text();
      var link = cheerio(this).find("a.link").attr('href');
      var extraInfo = cheerio(this).find("div.extra-info").length; // 투자유치 기록 있는 회사만 필터링
      if(typeof resultName !== '"ined' && resultName != null && typeof link !== 'undefined' && link != null && extraInfo > 0) {
        results.push({name: he.decode(resultName.trim()), link: he.decode(link)});
      }
    });

    return results;
  }

  this.getCompanyDetails = async function(companyDetailUrl) {
    if(authCookies == null) await this.login();
    var companyDetails = {};

    var rawOutput = await this.callRawWebsite('GET', companyDetailUrl, null, null);
    var htmlObject = cheerio.load(rawOutput.body);

    companyDetails.name = he.decode(htmlObject('div#company-name > h1').text().trim());

    var logo = htmlObject('div#company-header div.logo img.ui').attr('src');
    companyDetails.logo = he.decode(logo);

    var companyDescription = htmlObject('input#overview-plain').val();
    if(companyDescription) companyDetails.description = he.decode(companyDescription);

    var foundingDate = null;
    var companyInfoItems = htmlObject('div.company-info > div.items > div.item > div.title');
    for(var i = 0; companyInfoItems.length > i; i++) {
      if(cheerio(companyInfoItems[i]).text().trim() == '설립일') {
        var foundingDateString = cheerio(companyInfoItems[i]).parent().find('div.content').text().trim();
        var foundingDateParts = foundingDateString.split('/');
        foundingDate = foundingDateParts[0];
        break;
      }
    }
    if(foundingDate) companyDetails.foundingDate = he.decode(foundingDate);

    var productsObjectUrl = htmlObject('div.product-list-wrapper').attr('data-url');
    if(typeof productsObjectUrl !== 'undefined' && productsObjectUrl != null) {
      companyDetails.products = await this.getProducts(productsObjectUrl);
    } else {
      companyDetails.products = [];
    }

    var newsLink = htmlObject('section#company-news > div.segment.items').attr('data-url');
    companyDetails.news = [];
    if(newsLink) {
      companyDetails.news = await this.getNews(newsLink);
    }

    var phoneNumber = htmlObject('div#company-phone').attr('data-phone');
    if(phoneNumber) companyDetails.phoneNumber = he.decode(phoneNumber);
    else phoneNumber = '';

    var companyTags = [];
    var companyTagObject = htmlObject('div.company-tag div.content a');
    var companyTagUrl = await companyTagObject.each(function(i, elm) {
      var tag = cheerio(this).text();
      companyTags.push(he.decode(tag));
    });
    companyDetails.tags = companyTags;

    return companyDetails;
  }

  this.getProducts = async function(productsUrl) {
    if(authCookies == null) await this.login();
    var rawOutput = await this.callRawWebsite('GET', productsUrl, {limit: 10}, null);
    var resultObject = JSON.parse(rawOutput.body);
    var htmlObject = cheerio.load(resultObject.data.template);
    var productObjects = htmlObject('div.product > div.content');
    var products = [];
    await productObjects.each(async function(i, elm) {
      var product = {};
      product.name = he.decode(cheerio(this).find('h5').find('span').remove().end().text().trim());
      product.header = he.decode(cheerio(this).find('div.header').text().trim());
      product.description = he.decode(cheerio(this).find('div.overview').text().trim());

      var productWebsite = cheerio(this).find('a.product-homepage');
      if(productWebsite.length > 0) product.website = he.decode(productWebsite.attr('href'));
      else product.website = '';
      var appStoreLink = cheerio(this).find('div.product-social-list > div.list > a.item > i.ic-apple');
      if(appStoreLink.length > 0) product.appStore = he.decode(appStoreLink.parent().attr('href'));
      var googlePlayLink = cheerio(this).find('div.product-social-list > div.list > a.item > i.ic-google_play');
      if(googlePlayLink.length > 0) product.playStore = he.decode(googlePlayLink.parent().attr('href'));
      var facebookLink = cheerio(this).find('div.product-social-list > div.list > a.item > i.ic-social-facebook');
      if(facebookLink.length > 0) product.facebook = he.decode(facebookLink.parent().attr('href'));
      products.push(product);
    });
    return products;
  }

  this.getNews = async function(sourceLink) {
    if(authCookies == null) await this.login();
    var rawOutput = await this.callRawWebsite('GET', sourceLink);
    var resultObject = JSON.parse(rawOutput.body);
    var htmlObject = cheerio.load(resultObject.data.template);
    var newsItems = htmlObject('.news.item');
    var news = [];
    for(var i = 0; newsItems.length > i; i++) {
      var link = cheerio(newsItems[i]).find('.content > a').attr('href');
      var headline = cheerio(newsItems[i]).find('.content > a > div').text().trim();
      var media = cheerio(newsItems[i]).find('.content > span.publisher').text().trim();
      var date = cheerio(newsItems[i]).find('.content > span').not('.publisher').text().trim().replace(/\./g, '-');
      if(!link) continue;
      news.push({
        link: he.decode(link),
        headline: he.decode(headline),
        media: he.decode(media),
        date: date
      });
    }
    return news;
  }
}

// Public
module.exports = Rocketpunch;
