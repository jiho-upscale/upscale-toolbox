'use strict';

var Saramin = new function() {

  const request = require('request-promise-native');
  const socksAgent = require('socks-proxy-agent');
  const useragent = require('random-useragent');
  const cheerio = require("cheerio");
  const utils = require('./utils.js');
  const queryEncode = require("querystring").encode;
  const he = require('he');
  const host = 'http://www.saramin.co.kr';
  const retryLimit = 50;
  var retryCount = 0;
  var currentProxy;

  const endpoint = {
    searchCompany: '/zf_user/search/company',
    companyDetail: '/zf_user/company-info/view'
  }

  var getProxy = async function() {
    try {
      var proxyInfo = await utils.getProxy();
      var agent = new socksAgent(proxyInfo.curl);
      currentProxy = proxyInfo.curl;
      console.log('[saramin] proxy is: ' + currentProxy);
      return agent;
    } catch(error) {
      console.log(error);
      return getProxy();
    }
  }

  var callRawWebsite = async function(method, endpoint, query, body) {
    var url = host + endpoint;
    if(query != null) url += '?' + queryEncode(query);
    console.log('[saramin] call ' + url);

    var options = {
      method: method.toUpperCase(),
      url: url,
      headers: {
        'User-Agent': useragent.getRandom(function(data) {
          if(data.folder == '/Browsers - Mac') return true;
          else return false;
        })
      },
      agent: await getProxy(),
      timeout: 3000,
      resolveWithFullResponse: true
    };

    if(method.toUpperCase() == 'POST' && body != null) {
      options.formData = body;
    }

    var result = request(options).then(function(result) {
      if(result == null || typeof result == 'undefined') {
        throw result;
      } else {
        retryCount = 0;
        return result.body;
      }
    }).catch(function(error) {
      if(error.response && (error.response.statusCode == 404 || error.response.statusCode == 500)) {
        throw error;
      }

      var badProxies = utils.cache().get('badProxies');
      if(badProxies == null) badProxies = [];
      badProxies.push(currentProxy);
      utils.cache().set('badProxies', badProxies);

      if(retryLimit > retryCount) {
        retryCount++;
        console.log('retry saramin : ' + retryCount);
        return callRawWebsite(method, endpoint, query, body);
      } else {
        console.log(error);
        process.exit();
      }
    });
    return Promise.resolve(result);
  }
  this.callRawWebsite = callRawWebsite;

  this.search = async function(companyName, ceoName) {
    var rawOutput = await this.callRawWebsite('GET', endpoint.searchCompany, {searchword: companyName}, null);
    var htmlObject = cheerio.load(rawOutput);
    var rows = htmlObject('div#company_info_list div.item_corp');

    var results = [];
    for(var i = 0; rows.length > i; i++) {
      var result = {};
      var row = cheerio(rows[i]);
      var name = row.find('h2.corp_name > a.company_popup > b').text().trim();
      var infoCells = row.find('div.corp_info > dl');
      var ceoNameResult;
      for(var x = 0; infoCells.length > x; x++) {
        var label = cheerio(infoCells[x]).find('dt').text().trim();
        if(label == '대표자명') {
          ceoNameResult = cheerio(infoCells[x]).find('dd').text().trim();
        }
      }

      if(companyName.trim() == name && ceoName.trim() == ceoNameResult) {
        result.name = he.decode(name);
        result.link = he.decode(row.find('h2.corp_name > a.company_popup').attr('href'));
        results.push(result);
      }
    }
    return results;
  }

  this.getCompanyDetails = async function(link) {
    var rawOutput = await this.callRawWebsite('GET', link, null, null);
    var htmlObject = cheerio.load(rawOutput);
    var result = {};
    var logo = htmlObject('div.header_info > div.title_info > div.thumb_company img').attr('src');
    var name = htmlObject('div.info_company span.name').text();
    var website = htmlObject('a.link_url').attr('href');
    if(logo) result.logo = he.decode(logo);
    else result.logo = '';
    result.name = he.decode(name);
    if(website) result.website = he.decode(website);
    else result.website = '';
    return result;
  }
}

// Public
module.exports = Saramin;
